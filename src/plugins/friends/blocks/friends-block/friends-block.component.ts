import { BlockService } from '../../../../app/services/block.service';
import { Component, Inject, Input, ViewEncapsulation, OnInit, OnChanges } from '@angular/core';
import { Block } from '../../../../app/models/block.model';
import { Profile } from '../../../../app/models/profile.model';

@Component({
  selector: "noosfero-friends-block",
  templateUrl: './friends-block.html',
  styleUrls: ['./friends-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FriendsBlockComponent implements OnInit, OnChanges {

  @Input() block: Block;
  @Input() owner: Profile;
  @Input() designMode: boolean;
  public loading = false;

  profiles: any = [];
  constructor(private blockService: BlockService) { }

  ngOnInit() {
    if (!this.loading) {
      this.getBlockContent()
    }
  }

  getBlockContent() {
    this.loading = true;
    const limit: number = ((this.block && this.block.settings) ? this.block.settings.limit : null) || 4;
    if (this.block.api_content) {
      this.profiles = this.block.api_content['people'];
      this.loading = false;
    } else {
      this.blockService.getBlock(this.block).subscribe((block: Block) => {
        this.block.api_content = block.api_content;
        this.profiles = this.block.api_content.people;
        this.loading = false;
      });
    }

  }

  isEnvironment() {
    return this.owner && this.owner.type === "Environment";
  }

  ngOnChanges() {
    if (!this.loading) {
      this.getBlockContent()
    }
  }
}
