import { BlockService } from '../../../../app/services/block.service';
import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { Block } from '../../../../app/models/block.model';

// FIXME add all events present on folder activities and remove all code in this folder

@Component({
  selector: "noosfero-recent-activities-plugin-activities-block",
  templateUrl: './recent-activities-plugin-activities-block.html',
  styleUrls: ['./recent-activities-plugin-activities-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RecentActivitiesPluginActivitiesBlockComponent implements OnInit {

  @Input() block: any;
  @Input() owner: any;
  @Input() designMode: boolean;

  profile: any;
  activities: any;

  constructor(private blockService: BlockService) { }

  ngOnInit() {
    this.profile = this.owner;
    this.activities = [];
    this.blockService.getBlock(this.block).subscribe((block: Block) => {
      this.activities = block.api_content.activities;
    });
  }
}
