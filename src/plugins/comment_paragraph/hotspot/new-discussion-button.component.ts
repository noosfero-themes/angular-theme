import { PersonActionsHotspotInterface } from '../../../app/hotspot/person-actions-hotspot.component';
import { Input, Inject, Injector, Component, ViewEncapsulation } from '@angular/core';
import { Hotspot } from '../../../app/hotspot/hotspot.decorator';
import { Profile } from '../../../app/models/profile.model';

@Component({
  selector: "new-discussion-button-hotspot",
  templateUrl: './new-discussion-button.html',
  styleUrls: ['./new-discussion-button.scss'],
})
@Hotspot("person_actions")
export class NewDiscussionButtonHotspotComponent implements PersonActionsHotspotInterface {

  profile: Profile;

  constructor() { }

  isPerson() {
    return this.profile.type === 'Person';
  }

}
