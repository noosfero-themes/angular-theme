export const translation = {
  "comment-paragraph-plugin.title": "Paragraph Comments",
  "comment-paragraph-plugin.export": "Export Comments",
  "comment-paragraph-plugin.discussion.editor.start_date.label": "From",
  "comment-paragraph-plugin.discussion.editor.end_date.label": "To",
  "comment-paragraph-plugin.discussion.header": "Open for comments",
  "comment-paragraph-plugin.discussion.notOpened.header": "Discussion will start {{date}}",
  "comment-paragraph-plugin.discussion.available.header": "Discussion will end {{date}}",
  "comment-paragraph-plugin.discussion.available.without-end.header": "Discussion opened",
  "comment-paragraph-plugin.discussion.closed.header": "Discussion finished {{date}}",
  "comment-paragraph-plugin.discussion.total_items": "Choose how many items will be displayed ",
  "comment-paragraph-plugin.discussion.which_display": "Choose which discussion should be displayed",
  "comment-paragraph-plugin.discussion.display.not_opened_discussions": "Not Opened Discussions",
  "comment-paragraph-plugin.discussion.display.available_discussions": "Available Discussions",
  "comment-paragraph-plugin.discussion.display.closed_discussions": "Closed Discussions",
  "comment-paragraph-plugin.discussion.view_all": "View All",
  "article.basic_editor.Discussion.success.message": "Discussion was sucessfully saved!"
}
