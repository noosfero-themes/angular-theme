export const translation = {
  "comment-paragraph-plugin.title": "Comentários por Parágrafo",
  "comment-paragraph-plugin.export": "Exportar Comentários",
  "comment-paragraph-plugin.discussion.editor.start_date.label": "De",
  "comment-paragraph-plugin.discussion.editor.end_date.label": "Até",
  "comment-paragraph-plugin.discussion.header": "Aberto para comentários",
  "comment-paragraph-plugin.discussion.notOpened.header": "Discussão iniciará {{date}}",
  "comment-paragraph-plugin.discussion.available.header": "Discussão terminará {{date}}",
  "comment-paragraph-plugin.discussion.available.without-end.header": "Discussão aberta",
  "comment-paragraph-plugin.discussion.closed.header": "Discussão finalizada {{date}}",
  "comment-paragraph-plugin.discussion.total_items": "Quantidade de itens a serem exibidos",
  "comment-paragraph-plugin.discussion.which_display": "Esacolha qual discussão deve ser exibida",
  "comment-paragraph-plugin.discussion.display.not_opened_discussions": "Discussões não abertas",
  "comment-paragraph-plugin.discussion.display.available_discussions": "Discussões disponíveis",
  "comment-paragraph-plugin.discussion.display.closed_discussions": "Discussões fechadas",
  "comment-paragraph-plugin.discussion.view_all": "Ver todas",
  "article.basic_editor.Discussion.success.message": "Discussão salva com sucesso!"
}
