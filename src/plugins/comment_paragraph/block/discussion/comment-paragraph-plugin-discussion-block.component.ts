import { Component, Inject, Input, ViewEncapsulation, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';
import * as _ from "lodash";

import { BlockService } from '../../../../app/services/block.service';
import { ArticleService } from '../../../../app/services/article.service';
import { DiscussionPresentationMode } from './discussion-presentation-mode';
import { Article } from '../../../../app/models/article.model';
import { Profile } from '../../../../app/models/profile.model';
import { Block } from '../../../../app/models/block.model';
import { DiscussionStatus } from './discussion-status';
import { CommentParagraphPluginDiscussionBlockSettingsComponent } from './comment-paragraph-plugin-discussion-block-settings.component';
import { Level } from '../../../../app/models/level.model';

@Component({
  selector: "noosfero-comment-paragraph-plugin-discussion-block",
  templateUrl: './comment-paragraph-plugin-discussion-block.html',
  styleUrls: ['./comment-paragraph-plugin-discussion-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CommentParagraphPluginDiscussionBlockComponent implements OnInit, OnChanges {

  @Input() block: any;
  @Input() owner: any;
  @Input() designMode: boolean;

  profile: Profile;
  documents: Article[];
  fixedDocuments: Article[];
  total_items = 0;
  presentation_mode: DiscussionPresentationMode;
  discussion_status: number;
  public loading = false;
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  startDate = new Date();

  constructor(private blockService: BlockService, public articleService: ArticleService) { }

  ngOnInit() {
    this.profile = this.owner;

    if (!this.loading) {
      this.getBlockContent()
    }
    this.presentation_mode = DiscussionPresentationMode.TITLE_ONLY;
    if (this.block && this.block.settings && this.block.settings.presentation_mode) {
      this.presentation_mode = this.block.settings.presentation_mode;
    }
    this.articleService.articleRemoved.subscribe((article: Article) => {
      _.reject(this.documents, article);

    });
  }

  // FIXME make this unit tests
  addFixedDocuments(documents: Article[]) {
    this.fixedDocuments = _.union(documents, this.fixedDocuments);
    this.documents = _.union(this.fixedDocuments, this.documents).slice(0, this.total_items);
    this.updateBlock();
  }

  // FIXME make this unit tests
  isDocumentFixed(document: Article) {
    return !_.isNil(_.find(this.fixedDocuments, { id: document.id }));
  }

  // FIXME make this unit tests
  removeFixed(article: Article) {
    let index = this.fixedDocuments.indexOf(article, 0);
    this.fixedDocuments.splice(index, 1);

    index = this.documents.indexOf(article, 0);
    this.documents.splice(index, 1);
    this.updateBlock();
  }

  // FIXME make this unit tests
  getBlockContent() {
    this.loading = true;
    if (this.block.api_content) {
      this.fixedDocuments = this.block.api_content.fixed_documents;
      this.total_items = this.block.api_content.total_items;
      this.discussion_status = this.block.api_content.discussion_status;
      this.documents = _.union(this.fixedDocuments, this.block.api_content.articles);
      this.loading = false;

    } else {

      this.blockService.getBlock(this.block).subscribe((block: Block) => {
        this.fixedDocuments = block.api_content.fixed_documents;
        this.total_items = block.api_content.total_items;
        this.discussion_status = block.api_content.discussion_status;
        this.documents = _.union(this.fixedDocuments, block.api_content.articles);
        this.block._force_update = true;
        this.block.api_content = block.api_content;
        this.loading = false;

        if (!this.documents || this.documents.length === 0) {
          this.hideBlockChanged.emit(true)
        }

        this.loading = false;
      });
    }

  }

  updateBlock() {
    this.block.api_content.fixed_documents_ids = _.map(this.fixedDocuments, 'id');
  }

  ngOnChanges() {
    if (!this.loading) {
      this.getBlockContent()
    }
  }

  presentAbstract() {
    return this.presentation_mode === DiscussionPresentationMode.TITLE_AND_ABSTRACT;
  }

  presentFullContent() {
    return this.presentation_mode === DiscussionPresentationMode.FULL_CONTENT;
  }

  viewAllParams() {
    const params = { content_type: 'CommentParagraphPlugin::Discussion' };
    switch (this.discussion_status) {
      case DiscussionStatus.STATUS_NOT_OPENED:
        params['from_start_date'] = new Date();
        break;
      case DiscussionStatus.STATUS_CLOSED:
        params['until_end_date'] = new Date();
        break;
      case DiscussionStatus.STATUS_AVAILABLE:
        params['until_start_date'] = new Date();
        params['from_end_date'] = new Date();
        break;
    }

    return params;
  }

  viewAllBasePath() {
    let basePath = ['/', 'search'];
    if (this.isAProfile()) {
      basePath = ['/', 'profile', this.profile.identifier, 'search'];
    }
    return basePath;
  }

  isAProfile(): boolean {
    let isProfile = false;
    if (!_.isNil(this.profile) && this.profile.type !== 'Environment') {
      isProfile = true;
    }
    return isProfile;

  }

  isPrivate(article: Article): boolean {
    return article.access === Level.SELF;
  }

}
