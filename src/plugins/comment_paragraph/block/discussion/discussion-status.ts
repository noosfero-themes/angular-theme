
export enum DiscussionStatus {
  STATUS_NOT_OPENED = 0,
  STATUS_AVAILABLE = 1,
  STATUS_CLOSED = 2
}
