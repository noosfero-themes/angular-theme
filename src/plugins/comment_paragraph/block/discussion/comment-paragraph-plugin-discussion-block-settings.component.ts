import { Input, Inject, Component, OnInit, OnChanges, forwardRef, Injector, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { BlockService } from '../../../../app/services/block.service';
import { Environment } from '../../../../app/models/environment.model';
import { Profile } from '../../../../app/models/profile.model';
import { Block } from '../../../../app/models/block.model';
import { DragulaService } from 'ng2-dragula';
import * as _ from "lodash";


@Component({
  selector: "noosfero-comment-paragraph-plugin-discussion-block-settings",
  templateUrl: './comment-paragraph-plugin-discussion-block-settings.html',
  styleUrls: ['./comment-paragraph-plugin-discussion-block-settings.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CommentParagraphPluginDiscussionBlockSettingsComponent implements OnInit {

  @Input() block: Block;
  @Input() owner: Profile | Environment;

  isCollapsed: any;
  total_items: number;
  discussion_status: number;

  constructor() {
    this.total_items = 0;
  }

  ngOnInit() {
    this.isCollapsed = false;

    if (this.block.api_content) {
      this.total_items = this.block.api_content.total_items;
      this.discussion_status = this.block.api_content.discussion_status;
    }
  }

  updateBlock() {
    this.block.api_content.total_items = this.total_items;
    this.block.api_content.discussion_status = this.discussion_status;
  }

}
