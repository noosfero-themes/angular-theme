import { ProfileService } from '../../services/profile.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Profile } from '../../models/profile.model';
import { HttpStatusResolver } from './http-status.resolver';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ProfileResolver implements Resolve<Profile> {

  constructor(private profileService: ProfileService, private router: Router, private httpStatusResolver: HttpStatusResolver) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Profile> | any {
    return this.profileService.getByIdentifier(route.params["profile"], { optional_fields: ['boxes'] }).pipe(
      map( (profile: Profile) => {
        this.profileService.setCurrentProfile(profile);
        return profile;
      } )
      ,
      catchError( (error) => {
        this.httpStatusResolver.set(error.status);
          this.router.navigate(['/', route.params["profile"], 'no-content-found']);
          return Observable.of({});
        }
      ));
  }

}
