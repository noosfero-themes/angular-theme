import { ProfileService } from '../../services/profile.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Profile } from '../../models/profile.model';

@Injectable()
export class HttpStatusResolver implements Resolve<number> {

  httpStatus: number;

  constructor(private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): number {
    return this.httpStatus;
  }

  set(status: number) {
    this.httpStatus = status;
  }

  get(): number {
    return this.httpStatus;
  }
}
