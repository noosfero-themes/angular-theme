import { EnvironmentService } from '../../services/environment.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Environment } from '../../models/environment.model';
import { map, catchError } from 'rxjs/operators';
import { HttpStatusResolver } from './http-status.resolver';

@Injectable()
export class EnvironmentResolver implements Resolve<Environment> {

  constructor(private environmentService: EnvironmentService, private httpStatusResolver: HttpStatusResolver) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Environment> | any {
    const currentEnvironment = this.environmentService.getCurrentEnvironment();
    if (currentEnvironment) {
      return currentEnvironment;
    } else {
      return this.environmentService.get('default').pipe(
        map( (localEnvironment: Environment) => {
          this.environmentService.setCurrentEnvironment(localEnvironment);
          return localEnvironment;
        } )
        ,
        catchError( (error) => {
          this.httpStatusResolver.set(error.status);
            return Observable.of({});
          }
        ));
    }

  }
}
