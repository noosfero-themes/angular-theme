import { ArticleService } from '../../../services/article.service';
import { By } from '@angular/platform-browser';
import { NoContentFoundComponent } from './no-content-found.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import * as helpers from '../../../../spec/helpers';
import { TranslateModule } from '@ngx-translate/core';
import { HttpStatusResolver } from '../../resolvers/http-status.resolver';
import { RouterTestingModule } from '@angular/router/testing';

describe("NoContentFoundComponent Component", () => {
  let fixture: ComponentFixture<NoContentFoundComponent>;
  let component: NoContentFoundComponent;
  const mocks = helpers.getMocks();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NoContentFoundComponent],
      providers: [
        { provide: ArticleService, useValue: mocks.articleService },
        HttpStatusResolver,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [TranslateModule.forRoot(), RouterTestingModule]
    });
    fixture = TestBed.createComponent(NoContentFoundComponent);
    component = fixture.componentInstance;
  }));

  it("renders the not found page", () => {
    expect(fixture.debugElement.queryAll(By.css('.no-content-found')).length).toEqual(1);
  });
});

