import { Profile } from '../../../models/profile.model';
import { Component, Input, OnInit } from '@angular/core';
import * as _ from "lodash";
import { HttpStatusResolver } from '../../resolvers/http-status.resolver';
import { environment } from '../../../../environments/environment';
import { Environment } from '../../../models/environment.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: "no-content-found",
  templateUrl: './no-content-found.html',
  styleUrls: ['no-content-found.scss']
})
export class NoContentFoundComponent implements OnInit {

  @Input() profile: Profile;
  @Input() httpStatus: number;
  environment: Environment;

  constructor(private httpStatusResolver: HttpStatusResolver, private activatedRoute: ActivatedRoute) {
    this.environment = this.activatedRoute.snapshot.data['environment'];
  }

  ngOnInit() {
    const status = this.httpStatusResolver.get();
    if (_.isNil(this.httpStatus)) {
      this.httpStatus = _.isNil(status) ? 404 : status;
    }
  }

}


