import { Pipe, Inject, PipeTransform } from '@angular/core';

@Pipe({ name: 'noosferoRouterLink', pure: false })
export class NoosferoRouterLink implements PipeTransform {

  transform(viewObject: any) {
    let url = '/' + viewObject.profile;
    if (viewObject.page) {
      url += '/' + viewObject.page.join('/');
    }
    return url;
  }

}
