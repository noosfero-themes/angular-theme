import { Injectable } from '@angular/core';
import { ModelWithPermissions } from '../../models/model-with-permissions.model';

@Injectable()
export class PermissionService {
  isAllowed(target: ModelWithPermissions, permission: string) {
    return (target.permissions || []).indexOf(permission) >= 0;
  }

}
