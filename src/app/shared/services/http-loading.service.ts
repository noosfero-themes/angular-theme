import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class HttpLoadingService {

  isLoadingChanged: Subject<boolean> = new Subject<boolean>();
  startedRequests = 0;
  finishedRequests = 0;

  started() {
    this.startedRequests += 1;
    this.isLoadingChanged.next(this.startedRequests === this.finishedRequests);
  }

  finished() {
    this.finishedRequests += 1;
    this.isLoadingChanged.next(this.startedRequests === this.finishedRequests);
  }

};
