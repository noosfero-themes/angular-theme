import { environment } from '../environments/environment';
import { BodyStateClassesService } from './shared/services/body-state-classes.service';
import { ChangeDetectorRef, AfterViewInit, Inject, ViewEncapsulation, Component } from '@angular/core';
import * as utils from './shared/utils';
import { HttpLoadingService } from './shared/services/http-loading.service';

@Component({
  selector: 'noosfero-app',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements AfterViewInit {

  public themeSkin = 'skin-default';
  private appClasses = "";

  constructor(@Inject('Window') private window,
    private bodyStateClassesService: BodyStateClassesService,
    private httpLoadingService: HttpLoadingService,
    private cdr: ChangeDetectorRef) {
    bodyStateClassesService.start({
      skin: this.themeSkin
    });
    window['CKEDITOR_BASEPATH'] = `${environment.assetsPath}/ckeditor/`;
    utils.loadScript(`${environment.assetsPath}/ckeditor/ckeditor.js`);
  }

  ngAfterViewInit() {
    this.bodyStateClassesService.changeClasses.subscribe((classes: string[]) => {
      this.appClasses = classes.join(" ");
      this.cdr.detectChanges();
    });
  }

  requestStarted() {
    this.httpLoadingService.started();
  }

  requestFinished() {
    this.httpLoadingService.finished();
  }
}
