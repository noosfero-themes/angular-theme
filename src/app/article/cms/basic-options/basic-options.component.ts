import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Article } from '../../../models/article.model';
import { Level } from '../../../models/level.model';

@Component({
  selector: 'article-basic-options',
  templateUrl: './basic-options.html',
  styleUrls: ['./basic-options.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BasicOptionsComponent {

  @Input() article: Article;

  private(): number {
    return Level.SELF;
  }

  public(): number {
    return Level.VISITORS;
  }

}
