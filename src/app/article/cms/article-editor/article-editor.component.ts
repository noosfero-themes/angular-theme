import { SessionService } from '../../../services/session.service';
import { Component, Input, Inject, OnInit } from '@angular/core';
import { Article } from '../../../models/article.model';
import { Profile } from '../../../models/profile.model';

@Component({
    selector: 'article-editor',
    templateUrl: './article-editor.html'
})
export class ArticleEditorComponent implements OnInit {

    @Input() article: Article;
    @Input() profile: Profile;
    @Input() path: string;
    options: any;

    constructor(@Inject("Window") private window: Window,
        private sessionService: SessionService) { }

    ngOnInit() {
        this.window['uploadUrl'] = '/api/v1/profiles/' + this.profile.id + '/uploaded_files';
        this.window['listUrl'] = '/api/v1/profiles/' + this.profile.id + '/articles?content_type=Folder,UploadedFile&parent_id=';
        this.window['deleteUrl'] = '/api/v1/articles';
        this.window['renameUrl'] = '/api/v1/articles';
        this.window['apiRootKey'] = 'article';
        this.window['privateToken'] = this.sessionService.currentUser().private_token;
        this.window['baseUrl'] = this.getDomanWithPort();
        this.options = { allowedContent: true, removeFormatAttributes: '', filebrowserBrowseUrl: '/ngx-filemanager?editor=CKEditor' };
        this.options['toolbarGroups'] = [
            { name: 'document', groups: ['mode', 'document', 'doctools'] },
            { name: 'clipboard', groups: ['clipboard', 'undo'] },
            { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
            { name: 'tools', groups: ['tools'] },
            '/',
            { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
            { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
            { name: 'links', groups: ['links'] },
            '/',
            { name: 'styles', groups: ['styles'] },
            { name: 'colors', groups: ['colors'] },
            { name: 'others', groups: ['others'] },
            { name: 'insert', groups: ['insert'] },
        ];
        this.options['removeButtons'] = 'Smiley,Form,Radio,TextField,Checkbox,Textarea,Select,Button,ImageButton,HiddenField,About,Language,CreateDiv,Flash,Iframe,PageBreak';

        // this.options['baseHref'] = 'http://localhost:4200/educultura';
        // put contents after change angular body parser
        this.options['extraPlugins'] = 'autoembed,codesnippet,colorbutton,colordialog,copyformatting,dialog,docprops';
        this.options['extraPlugins'] = this.options['extraPlugins'] + ',preview,print,selectall,showblocks,pastefromword';
        this.options['extraPlugins'] = this.options['extraPlugins'] + ',embedbase,embedsemantic,find,font,image,justify,liststyle';
        this.options['extraPlugins'] = this.options['extraPlugins'] + ',widget,wsc';
    }

    getDomanWithPort() {
        return location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
    }
}
