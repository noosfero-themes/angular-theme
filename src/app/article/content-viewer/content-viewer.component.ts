import { Component } from '@angular/core';
import { Location } from '@angular/common';
import * as _ from "lodash";

import { ArticleService } from '../../services/article.service';
import { ProfileService } from '../../services/profile.service';
import { AuthService } from '../../services';
import { Profile } from '../../models/profile.model';
import { Article } from '../../models/article.model';
import { ActivatedRoute, Router, Event, NavigationStart } from '@angular/router';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: "content-viewer",
  templateUrl: './page.html',
})
export class ContentViewerComponent {

  article: Article = null;
  profile: Profile = null;
  loading = false;
  httpStatus: number;

  constructor(private articleService: ArticleService, private profileService: ProfileService,
    public authService: AuthService, private location: Location,
    private router: Router,
    private activatedRoute: ActivatedRoute, private notificationService: NotificationService) {

    this.loadArticle();

    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.loadArticle(event.url);
      }
    });

    this.profileService.profileChangeEvent.subscribe(profile => {
      this.profile = profile;
    });

    this.authService.loginSuccess.subscribe(() => {
      this.loadArticle();
    });
    this.authService.logoutSuccess.subscribe(() => {
      this.loadArticle();
    });
  }

  noContentFound(): boolean {
    return (!this.article && !this.loading);
  }

  isContentUrl(url: string): boolean {
    let isContent = true;

    if (url.match('/profile/' + this.profile.identifier)) {
      isContent = false
    } else if (url.match('/myprofile/' + this.profile.identifier)) {
      isContent = false
    }
    return isContent
  }

  loadArticle(url = '') {
    this.loading = true;

    this.profile = this.profileService.getCurrentProfile();
    if (_.isNil(url) || url === '') {
      url = this.location.path();
      if (url === '/' + this.profile.identifier) {
        url = this.activatedRoute.snapshot.paramMap.get('path')
      }
    } else if (!this.isContentUrl(url)) {
      this.loading = false;
      return null;
    }

    const page = _.replace(url, '/' + this.profile.identifier + '/', '');

    this.articleService.getArticleByProfileAndPath(this.profile, page).subscribe(
      (article: Article) => {
        this.article = article;
        this.articleService.setCurrent(this.article);
        this.loading = false;
        window.scrollTo(0, 0);
      }, (error: any) => {
        this.httpStatus = error.status;
        if (error.status === 403) {
          this.notificationService.error({ title: "notification.error.default.title", message: "notification.http_error.403.message" });
        }
        this.loading = false;
      });
  }
}
