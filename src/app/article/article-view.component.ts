import { Input, Inject, Component, Directive, ViewEncapsulation } from '@angular/core';

import { Article } from '../models/article.model';
import { Profile } from '../models/profile.model';

@Component({
  selector: 'noosfero-article',
  templateUrl: './article-view.html',
  styleUrls: ['article.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ArticleViewComponent {

  @Input() article: Article;
  @Input() profile: Profile;

}
