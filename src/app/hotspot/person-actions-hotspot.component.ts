import { HotspotModule } from './hotspot.module';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, Input, NgModuleFactory } from '@angular/core';
import { PluginHotspot } from './plugin-hotspot';
import * as plugins from '../../plugins';
import { Profile } from '../models/profile.model';

export interface PersonActionsHotspotInterface {
  profile: Profile;
}

@Component({
  selector: "noosfero-hotspot-person-actions",
  template: ''

})
export class PersonActionsHotspotComponent extends PluginHotspot {

  @Input() profile: Profile;
  hotspotComponent: any;
  myModule: any;

  constructor(private viewContainerRef: ViewContainerRef, private factory: ComponentFactoryResolver) {
    super('person_actions');
  }

  addHotspot(component: any) {
    const compFactory = this.factory.resolveComponentFactory(component);
    const componentRef = (this.viewContainerRef.createComponent(compFactory) as ComponentRef<PersonActionsHotspotInterface>);
    componentRef.instance.profile = this.profile;
    componentRef.changeDetectorRef.detectChanges();
  }
}
