import { TaskModule } from '../task.module';
import { ViewEncapsulation, OnInit, OnChanges } from '@angular/core';
import { components } from '../../../../themes';
import { AppModule } from '../../app.module';
import { Component, Input, Inject } from '@angular/core';
import { NotificationService } from '../../shared/services/notification.service';
import { TaskService } from '../../services/task.service';
import * as _ from "lodash";
import { Task } from '../../models/task.model';

@Component({
  selector: "task-list",
  templateUrl: './task-list.html',
  styleUrls: ['./task-list.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TaskListComponent implements OnInit, OnChanges {

  @Input() tasks: Task[];

  currentTask: Task;
  tasksGroups: Task[][];
  showAcceptModal = false;
  showRejectModal = false;

  constructor(private notificationService: NotificationService, private taskService: TaskService) {
  }

  ngOnInit() {
    this.taskService.taskClosed.subscribe((task: Task) => {
      this.tasks = _.reject(this.tasks, { id: task.id });
      this.tasksGroups = _.values(_.groupBy(this.tasks, 'target.name'));
    });
  }

  ngOnChanges() {
    this.tasks = _.sortBy(this.tasks, 'created_at').reverse();
    this.tasksGroups = _.values(_.groupBy(this.tasks, 'target.name'));
  }

  accept(task: Task, confirm = false) {
    if (task.accept_details && !confirm) {
      this.currentTask = task;
      this.showAcceptModal = true;
    } else {
      this.taskService.finishTask(task).subscribe((localTask) => {
        this.taskService.taskClosed.next(localTask);
        this.notificationService.success({ title: "tasks.actions.accept." + task.type + ".title", message: "tasks.actions.accept." + task.type + ".message" });
        this.cancel();
      }, (response) => {
        const error = response.error;

        if (_.isNil(error.errors)) {
          this.notificationService.error({ title: "tasks.actions.accept.error.title", message: error.message });
        } else {
          Object.keys(error.errors).forEach((key) => {
            error.errors[key].forEach((value) => {
              this.notificationService.error({
                title: "tasks.actions.accept.error.title",
                message: "tasks.actions.accept.error." + key + '.' + value.error
              });
            });
          });
        }
      }
      );
    }
  }

  reject(task: Task, confirm = false) {
    if (task.reject_details && !confirm) {
      this.currentTask = task;
      this.showRejectModal = true;
    } else {
      this.taskService.cancelTask(task).subscribe((localTask) => {
        this.taskService.taskClosed.next(localTask);
        this.notificationService.success({ title: "tasks.actions.reject." + task.type + ".title", message: "tasks.actions.reject." + task.type + ".message" });
        this.cancel();
      });
    }
  }

  confirmAccept() {
    this.accept(this.currentTask, true)
  }

  confirmReject() {
    this.reject(this.currentTask, true)
  }

  cancel() {
    this.showAcceptModal = false;
    this.showRejectModal = false;
    this.currentTask = null;
  }

}
