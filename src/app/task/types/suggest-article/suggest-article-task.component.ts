import { Component } from '@angular/core';
import { TaskComponentInterface } from '../../task.component';
import { Task } from '../../../models/task.model';

@Component({
  selector: "suggest-article-task",
  templateUrl: './suggest-article.html'
})
export class SuggestArticleTaskComponent implements TaskComponentInterface {

  task: Task;
}
