import { TranslateModule } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Provider, Component } from '@angular/core';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { NgPipesModule } from 'ngx-pipes';
import { MomentModule } from 'angular2-moment';

import { TranslatorService } from '../../../shared/services/translator.service';
import { TaskService } from '../../../services/task.service';
import { TaskAcceptComponent } from '../../task-list/task-accept.component';
import * as helpers from '../../../../spec/helpers';
import { DateFormatPipe } from '../../../shared/pipes/date-format.pipe';
import { AbuseComplaintTaskAcceptComponent } from './abuse-complaint-task-accept.component';
import { AbuseComplaint } from '../../../models/abuse-complaint.model';

const htmlTemplate = '<abuse-complaint-task-accept [task]="ctrl.task"></abuse-complaint-task-accept>';

describe("Components", () => {

  describe("Abuse Complaint Task Accept Component", () => {

    const task = <AbuseComplaint>{
      api_content: {
        abuse_reports: [{ reason: 'Testing reason message!', reporter: { name: 'User Tester 1', created_at: '2016-01-02 12:30:00' } },
        { reason: 'Another testing reason message!', reporter: { name: 'User Tester 2', created_at: '2016-02-01 13:00:00' } }
        ]
      }
    };
    let fixture: ComponentFixture<AbuseComplaintTaskAcceptComponent>;
    let component: AbuseComplaintTaskAcceptComponent;
    const mocks = helpers.getMocks();

    beforeEach(async(() => {
      spyOn(mocks.taskService, 'get').and.returnValue(Observable.of(task));
      const taskAcceptComponent = { task: task, confirmationTask: {} };
      TestBed.configureTestingModule({
        imports: [NgPipesModule, MomentModule, TranslateModule.forRoot()],
        declarations: [AbuseComplaintTaskAcceptComponent, DateFormatPipe],
        providers: [
          { provide: TaskService, useValue: mocks.taskService },
          { provide: TranslatorService, useValue: mocks.translatorService },
          { provide: "amParseFilter", useValue: mocks.amParseFilter },
          { provide: TaskAcceptComponent, useValue: taskAcceptComponent },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      });
      fixture = TestBed.createComponent(AbuseComplaintTaskAcceptComponent);
      component = fixture.componentInstance;
      component.task = task;
    }));

    it("should the abuse complaint have more than one abuse report", () => {
      component.ngOnInit();
      expect(component.abuse_reports.length).toBe(2);
    });

    it("should not call taskservice get if task api_content is setted", fakeAsync(() => {
      component.task = <any>{ api_content: { abuse_reports: [] } };
      fixture.detectChanges();
      tick();
      expect(mocks.taskService.get).not.toHaveBeenCalled();
    }));

    it("should set task when task target is setted", fakeAsync(() => {
      component.task = <any>{ target: null };
      component.ngOnInit();
      expect(component.task).toEqual(task);
    }));
  });
});
