import { TaskService } from '../../../services/task.service';
import { AbuseComplaint } from '../../../models/abuse-complaint.model';
import { Task } from '../../../models/task.model';
import { TaskAcceptComponentInterface } from '../../task-list/task-accept.component';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, OnInit } from '@angular/core';
import * as _ from "lodash";
import { AbuseReport } from '../../../models/abuse-report.model';

@Component({
  selector: "abuse-complaint-task-accept",
  templateUrl: './abuse-complaint-accept.html',
})
export class AbuseComplaintTaskAcceptComponent implements TaskAcceptComponentInterface, OnInit {

  task: Task;
  abuse_reports: AbuseReport[];

  constructor(private taskService: TaskService) { }

  ngOnInit() {
    if (_.isNil(this.task.api_content)) {
      this.taskService.get(this.task.id, { optional_fields: ['abuse_reports'] }).subscribe((task: AbuseComplaint) => {
        this.task = task;
        this.abuse_reports = this.task.api_content.abuse_reports;
      });
    } else {
      this.abuse_reports = this.task.api_content.abuse_reports;
    }



  }
}
