import { ViewChild, HostListener, Component, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { AuthService } from '../../services';
import { Person } from '../../models/person.model';
import { Task } from '../../models/task.model';
import * as _ from "lodash";

@Component({
  selector: "tasks-menu",
  templateUrl: './tasks-menu.html',
  styleUrls: ['./tasks-menu.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TasksMenuComponent implements OnInit {

  @Input() taskTypes: any;
  @ViewChild("taskPanel") taskPanel: any;
  @ViewChild("menuButton") menuButton: any;

  open = false;
  tasks: Task[] = [];
  total: number;
  perPage = 5;
  person: Person;

  constructor(private taskService: TaskService, private authService: AuthService) {
    if (_.isNil(this.taskTypes)) {
      this.taskTypes = _.reject(TaskService.TASK_TYPES, function (o) { return (o === "AddFriend") });
    }
  }

  ngOnInit() {
    this.taskService.taskClosed.subscribe((task: Task) => {
      if (this.taskTypes.indexOf(task.type) !== -1) {
        this.total--;
      }
    });
    this.authService.loginSuccess.subscribe(() => {
      this.loadTasks();
    });
    this.loadTasks();
  }

  loadTasks() {
    this.person = this.authService.currentPerson();

    this.taskService.getAllPending({ content_type: this.taskTypes.join(), per_page: this.perPage }).subscribe((result: any) => {
      this.total = result.headers.get('total');
      this.tasks = result.body;
    });
  }

  @HostListener('document:click', ['$event'])
  onClick($event: any) {
    if (!this.menuButton) return;
    if (this.menuButton.nativeElement.contains($event.target)) {
      this.open = !this.open;
    } else if (this.open && !this.taskPanel.nativeElement.contains($event.target)) {
      this.open = false;
    }
  }
}
