import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { TaskService } from './task.service';
import { Task } from '../models/task.model';
import * as helpers from '../../spec/helpers';

describe("Services", () => {
    describe("Task Service", () => {
        let service: TaskService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            spyOn(mocks.sessionService, "destroy");
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    TaskService,
                ]
            });
            service = TestBed.get(TaskService);
        }));

        describe("Succesfull requests", () => {

            it("list pending tasks", () => {
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/tasks?all_pending=true&content_type=AddMember,ApproveComment,ApproveArticle,AbuseComplaint,SuggestArticle,CreateCommunity,AddFriend&status=1`,
                //     { tasks: [{ id: 1 }] }, {}, 200);
                service.getAllPending().subscribe((result: any) => {
                    expect(result.body).toEqual(<Task[]>[{ id: 1 }]);
                });
            });

            it("finish a task", () => {
                const taskId = 1;
                const task: Task = <any>{ id: taskId };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/tasks/${taskId}/finish`, { task: { id: taskId } }, {}, 200);
                service.finishTask(task).subscribe((result: any) => {
                    expect(result.body).toEqual(<Task>{ id: 1 });
                });
            });

            it("cancel a task", () => {
                const taskId = 1;
                const task: Task = <any>{ id: taskId };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/tasks/${taskId}/cancel`, { task: { id: taskId } }, {}, 200);
                service.cancelTask(task).subscribe((result: any) => {
                    expect(result.body).toEqual(<Task>{ id: 1 });
                });
            });
        });

    });
});
