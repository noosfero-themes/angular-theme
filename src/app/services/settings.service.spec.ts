import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MockBackend } from '@angular/http/testing';
import { async, TestBed } from '@angular/core/testing';

import { SettingsService } from './settings.service';
import { Profile } from '../models/profile.model';
import { BlockDefinition } from '../models/block-definition.model';
import * as helpers from '../../spec/helpers';

describe("Services", () => {
    describe("Settings Service", () => {
        let service: SettingsService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    SettingsService,
                ],
            });
            service = TestBed.get(SettingsService);
        }));

        describe("Succesfull requests", () => {
            it("should return available blocks", () => {
                const profileId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/settings/available_blocks`,
                //     [{type: "RawHTMLBlock"}], {}, 200);
                service.getAvailableBlocks(<Profile>{ id: profileId }).subscribe((blocks: BlockDefinition[]) => {
                    expect(blocks[0].type).toEqual("RawHTMLBlock");
                });
            });
        });
    });
});
