import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { async, TestBed } from '@angular/core/testing';

import { ProfileService } from './profile.service';
import { Profile } from '../models/profile.model';
import * as helpers from '../../spec/helpers';

describe("Services", () => {
    describe("Profile Service", () => {
        let service: ProfileService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    ProfileService,
                ]
            });
            service = TestBed.get(ProfileService);
        }));

        describe("Succesfull requests", () => {
            it("should return profile by its identifier", () => {
                const identifier = 'profile1';
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${identifier}?key=identifier`, { name: "profile1" }, {}, 200);
                service.getByIdentifier(identifier).subscribe((profile: Profile) => {
                    expect(profile).toEqual(jasmine.objectContaining({ name: "profile1" }));
                });
            });

            // FIXME see if it's usefull
            // it("should return the members of a profile", () => {
            //     const profileId = 1;
            //     helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/members`, [{ name: "profile1" }], {}, 200);
            //     service.getProfileMembers(profileId).subscribe((response: any) => {
            //         expect(response.data[0]).toEqual({ name: "profile1" });
            //     });
            // });

            it("should return the boxes of a profile", () => {
                const profile = <Profile>{ id: 1 };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profile.id}/boxes`, [{ position: 1 }], {}, 200);
                service.getBoxes(profile).subscribe((response: any) => {
                    expect(response.data[0]).toEqual({ position: 1 });
                });
            });

            it("should return activities of a profile", () => {
                const profile = <Profile>{ id: 1 };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profile.id}/activities`, [{ verb: "create_article" }], {}, 200);
                service.getActivities(profile).subscribe((response: any) => {
                    expect(response.data[0]).toEqual({ verb: "create_article" });
                });
            });

            it("should resolve the current profile", () => {
                const profile = <Profile>{ id: 1, identifier: "profile1" };
                service.setCurrentProfile(profile);
                expect(profile).toEqual(service.getCurrentProfile());
            });

            it("should return the profile home page", () => {
                const profile = <Profile>{ id: 1, identifier: "profile1" };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profile.id}/home_page`, { path: "/something" }, {}, 200);
                service.getHomePage(profile).subscribe((response: any) => {
                    expect(response.data).toEqual(jasmine.objectContaining({ path: "/something" }));
                });
            });

            it("should return the tags for a profile", () => {
                const profile = <Profile>{ id: 1, identifier: "profile1" };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profile.id}/tags`, [{ name: "teste", count: 1 }], {}, 200);
                service.getTags(profile).subscribe((response: any) => {
                    expect(response.data).toEqual(jasmine.objectContaining([{ name: "teste", count: 1 }]));
                });
            });

            it("should update the profile attributes", () => {
                const profileId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}`, { custom_header: "something" }, {}, 200);
                service.update(<any>{ id: profileId, custom_header: "something" }).subscribe((response: any) => {
                    expect(response.data.custom_header).toEqual("something");
                });
            });

            // FIXME see if it's usefull
            // it("should return the profile members", () => {
            //     const profileId = 1;
            //     helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/members`, [{id: 2}], {}, 200);
            //     service.getMembers(<any>{ id: profileId }).subscribe((response: any) => {
            //         expect(response.data).toEqual(jasmine.objectContaining([{ id: 2 }]));
            //     });
            // });

            // FIXME see if it's usefull
            // it("should return true if the person is a profile member", () => {
            //     const profileId = 1;
            //     helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/members`, [{id: 2}], {}, 200);
            //     service.isMember(<any>{ id: 2 }, <any>{ id: profileId }).then((response: any) => {
            //         expect(response.data).toEqual([{id: 2}]);
            //     });
            // });

            // FIXME see if it's usefull
            // it("should return false if the person is a profile member", () => {
            //     const profileId = 1;
            //     helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/members`, [], {}, 200);
            //     service.isMember(<any>{ id: 2 }, <any>{ id: profileId }).then((response: any) => {
            //         expect(response.data).toEqual([]);
            //     });
            // });

            it("should add member to profile", () => {
                const profileId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/members`, { pending: false }, {}, 200);
                service.addMember(<any>{ id: 2 }, <any>{ id: profileId }).subscribe((response: any) => {
                    expect(response.data.pending).toEqual(false);
                });
            });

            it("should remove member from profile", () => {
                const profileId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/members`, { id: 2 }, {}, 200);
                service.removeMember(<any>{ id: 2 }, <any>{ id: profileId }).subscribe((response: any) => {
                    expect(response.data).toEqual(jasmine.objectContaining({ id: 2 }));
                });
            });
        });
    });
});
