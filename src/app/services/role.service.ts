import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BackendBaseService } from './backend_base.service';
import { Role } from '../models/role.model';

@Injectable()
export class RoleService extends BackendBaseService {

    getByProfile(profileId: number, params: any = {}): Observable<Role[]> {
        const httpParams = new HttpParams({
            fromObject: params
          });
        return this.http.get<Role[]>(this.baseURL + '/profiles/' + profileId + '/roles', {params: httpParams});
    }

    assign(profileId: number, personId: number, roleIds: number[], removeRoleIds: number[]): Observable<Role[]> {
        return this.http.post<Role[]>(this.baseURL + '/profiles/' + profileId + '/roles/assign', { person_id: personId, role_ids: roleIds, remove_role_ids: removeRoleIds });
    }

}
