import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from "lodash";

import { BackendBaseService } from './backend_base.service';
import { Community } from '../models/community.model';
import { Person } from '../models/person.model';
import { Profile } from '../models/profile.model';
import { DefaultResponse } from '../models/default-response.model';
import { HttpResponse } from '@angular/common/http';

@Injectable()
export class CommunityService extends BackendBaseService {

    createNewCommunity(community: Community): Observable<Community> {
        const params: any = {
            community: Object.assign({}, _.omitBy(_.pick(community, ['name', 'closed']), _.isNull))
        };

        return this.http.post<Community>(this.baseURL  + '/communities/', params);
    }

    sendInvitations(community: Community, people: Person[]): Observable<DefaultResponse> {
        const invitations = [];
        for (const invitation of people) {
            invitations.push(`${invitation.id}`);
        }
        const params = { 'contacts': invitations };

        return this.http.post<DefaultResponse>(this.baseURL  + '/communities/' + community.id + '/invite', params);
    }

    getMembershipState(person: Person, profile: Profile): Observable<DefaultResponse> {
        if ( person) {
            const params = { identifier: person.identifier }
            return this.http.get<DefaultResponse>(this.baseURL + '/communities/' + profile.id + '/membership', {params: this.parseParams(params)});
        } else {
            return Observable.throw({})
        }
    }

    search(params: any): Observable<HttpResponse<Community[]>> {
        return this.http.get<Community[]>(this.baseURL + '/communities',  {observe: 'response', params: this.parseParams(params)});
    }
}
