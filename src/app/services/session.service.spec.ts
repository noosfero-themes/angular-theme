import { SessionService } from './session.service';
// import { fixtures } from './../../spec/helpers';
import { PersonService } from './person.service';
import * as helpers from '../../spec/helpers';
import { User } from '../models/user.model';

describe("Services", () => {
    describe("Session Service", () => {

        const mocks = helpers.getMocks();

        it("method 'create()' saves the current user on mocks.localStorageService service", () => {
            const session = new SessionService(<any>mocks.localStorageService);
            session.create(mocks.user);
            expect(mocks.localStorageService.retrieve('currentUser')).toEqual(mocks.user);
        });

        it("method 'destroy()' clean the currentUser on mocks.localStorageService", () => {
            const session = new SessionService(<any>mocks.localStorageService);
            mocks.localStorageService.store('currentUser', mocks.user);
            session.destroy();
            expect(mocks.localStorageService.retrieve('currentUser')).toBeUndefined();
        });

        it("method 'currentUser()' returns the user recorded on mocks.localStorageService service", () => {
            const session = new SessionService(<any>mocks.localStorageService);
            mocks.localStorageService.store('currentUser', mocks.user);
            expect(session.currentUser()).toEqual(mocks.localStorageService.retrieve('currentUser'));
        });

        it("method 'currentPerson()' returns the person recorded on mocks.localStorageService service", () => {
            const session = new SessionService(<any>mocks.localStorageService);
            mocks.localStorageService.store('currentUser', mocks.user);
            expect(session.currentPerson()).toEqual(mocks.localStorageService.retrieve('currentUser').person);
        });
    });

});
