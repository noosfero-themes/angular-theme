import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import * as _ from "lodash";
import { BackendBaseService } from "./backend_base.service";
import { Profile } from "../models/profile.model";
import { Scrap } from "../models/scrap.model";
import { Subject } from "rxjs";

@Injectable()
export class ScrapService extends BackendBaseService {
  scrapCreated: Subject<Scrap> = new Subject<Scrap>();

  post(profile: Profile, scrap: Scrap): Observable<Scrap> {
    return this.http.post<Scrap>(
      this.baseURL + "/profiles/" + profile.id + "/scraps",
      { scrap: scrap }
    );
  }
  getReplies(profile: Profile, scrap: Scrap): Observable<Scrap[]> {
    return this.http.get<Scrap[]>(
      this.baseURL +
        "/profiles/" +
        profile.id +
        "/scraps/" +
        scrap.id +
        "/replies"
    );
  }
}
