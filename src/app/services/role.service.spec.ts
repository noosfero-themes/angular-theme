import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';

import { RoleService } from './role.service';
import * as helpers from '../../spec/helpers';
import { Role } from '../models/role.model';

describe("Services", () => {
    describe("Role Service", () => {
        let service: RoleService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    RoleService
                ],
            });
            service = TestBed.get(RoleService);
        }));

        describe("Succesfull requests", () => {
            it("list organization roles", () => {
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/1/roles`,
                //     { roles: [{ id: 1 }] }, {}, 200);
                service.getByProfile(1).subscribe((result: any) => {
                    expect(result.data).toEqual(<Role[]>[{ id: 1 }]);
                });
            });
        });
    });
});
