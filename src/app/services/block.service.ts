import { Injectable, Inject, EventEmitter, Output } from '@angular/core';

import { Block } from '../models/block.model';
import { Profile } from '../models/profile.model';
import { BackendBaseService } from './backend_base.service';
import { Observable } from 'rxjs/Observable';
import { Environment } from '../models/environment.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class BlockService extends BackendBaseService {

    hideContent: EventEmitter<boolean> = new EventEmitter<boolean>();
    blockChanged: EventEmitter<Block> = new EventEmitter<Block>();
    blocksSaved: Subject<Block[]> =  new Subject<Block[]>();


    getBlock(block: Block, params?: any): Observable<Block> {
        return this.get(block.id, params);
    }

    get(blockId: number, params?: any): Observable<Block> {
        return this.http.get<Block>(this.baseURL + '/blocks/' + blockId, {params: this.parseParams(params)});
    }

    uploadImages(block: Block, base64ImagesJson: any): Observable<Block> {
        return this.http.post<Block>(this.baseURL  + '/blocks/' + block.id,  { block: { api_content: block.api_content, images_builder: base64ImagesJson }, optional_fields: ['images']  });
    }

}
