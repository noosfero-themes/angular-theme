import { Router, ActivatedRoute } from '@angular/router';
import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import * as _ from "lodash";
import { TabsetComponent } from 'ngx-bootstrap';
import { Profile } from '../../models/profile.model';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'profile-search',
  templateUrl: './profile-search.html',
  styleUrls: ['./profile-search.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProfileSearchComponent {

  profile: Profile;

  constructor(private profileService: ProfileService) {
    this.profile = this.profileService.getCurrentProfile();

    this.profileService.profileChangeEvent.subscribe(profile => {
      this.profile = profile;
    });
  }
}
