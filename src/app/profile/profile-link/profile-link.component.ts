import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Profile } from '../../models/profile.model';

@Component({
    selector: 'profile-link',
    templateUrl: './profile-link.html',
    styleUrls: ['./profile-link.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileLinkComponent {

    @Input() profile: Profile;
    @Input() displayImage = false;
    @Input() displayName = true;
    @Input() truncate = true;

}
