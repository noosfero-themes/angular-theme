import { NgPipesModule } from "ngx-pipes";
import { By } from "@angular/platform-browser";
import { TranslateModule } from "@ngx-translate/core";
import { RouterTestingModule } from "@angular/router/testing";
import { TestBed, ComponentFixture } from "@angular/core/testing";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import * as helpers from "../../../spec/helpers";
import { ScrapComponent } from "./scrap.component";
import { FormsModule } from "@angular/forms";
import { ScrapService } from "../../services/scrap.service";
import { DateFormatPipe } from "../../shared/pipes/date-format.pipe";

describe("Components", () => {
  describe("Scrap Component", () => {
    const mocks = helpers.getMocks();
    let fixture: ComponentFixture<ScrapComponent>;
    let component: ScrapComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterTestingModule,
          TranslateModule.forRoot(),
          FormsModule,
          NgPipesModule
        ],
        declarations: [ScrapComponent, DateFormatPipe],
        providers: [
          { provide: ScrapService, useValue: mocks.scrapService },
          { provide: "amParseFilter", useValue: mocks.amParseFilter },

        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(ScrapComponent);
      component = fixture.componentInstance;
    });

    it("render a noosfero activity tag for each activity", () => {
      fixture.detectChanges();
      expect(
        fixture.debugElement.queryAll(By.css(".scrap-leave")).length
      ).toEqual(1);
    });
  });
});
