import { Component, ViewEncapsulation, OnInit, Input } from "@angular/core";

import { Profile } from "../../models/profile.model";
import { Scrap } from "../../models/scrap.model";
import * as _ from "lodash";
import { ScrapService } from "../../services/scrap.service";

@Component({
  selector: "noosfero-scrap",
  templateUrl: "./scrap.html",
  styleUrls: ["./scrap.scss"],
  encapsulation: ViewEncapsulation.None
})
export class ScrapComponent implements OnInit {
  scrap: Scrap;
  page = 1;
  @Input() profile: Profile;
  hasActivities = false;
  MAX_CONTENT_SIZE = 415;

  constructor(private scrapService: ScrapService) {}

  ngOnInit() {
    this.scrap = new Scrap();
  }

  contentLeft(): number {
    let contentLeft = this.MAX_CONTENT_SIZE;
    if (!_.isNil(this.scrap) && !_.isNil(this.scrap.content)) {
      contentLeft = contentLeft - this.scrap.content.length;
    }
    return contentLeft;
  }

  save() {
    this.scrapService.post(this.profile, this.scrap).subscribe((scrap: Scrap) => {
      this.scrapService.scrapCreated.next(scrap);
      this.scrap = new Scrap();
    });

  }
}
