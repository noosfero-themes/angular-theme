import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import * as helpers from '../../../../../spec/helpers';
import { ActivityHeaderComponent } from './activity-header.component';
import { TranslateModule } from '@ngx-translate/core';
import { Activity } from '../../../../models/activity.model';
import { DateFormatPipe } from '../../../../shared/pipes/date-format.pipe';
import { MomentModule } from 'angular2-moment';

describe("Components", () => {
    describe("Noosfero Activity Header", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<ActivityHeaderComponent>;
        let component: ActivityHeaderComponent;

        beforeEach(() => {
            spyOn(mocks.environmentService, "getCurrentEnvironment").and.returnValue(Promise.resolve({}));
            TestBed.configureTestingModule({
                declarations: [ActivityHeaderComponent, DateFormatPipe],
                schemas: [NO_ERRORS_SCHEMA],
                imports: [TranslateModule.forRoot(), MomentModule]
            });
            fixture = TestBed.createComponent(ActivityHeaderComponent);
            component = fixture.componentInstance;
            component.profiles = 1;
            component.desc = "desc.single";
            component.activity = <Activity>{ user: { id: 1} };
        });

        it("get plural description", () => {
            component.profiles = 2;
            fixture.detectChanges();
            expect(component.getDesc()).toContain("plural");
        });

        it("get singular description", () => {
            component.profiles = null;
            fixture.detectChanges();
            expect(component.getDesc()).not.toContain("plural");
        });

        it("verify if profiles count greater then zero", () => {
            component.profiles = null;
            fixture.detectChanges();
            expect(component.count()).toBeGreaterThan(0);
        });
    });
});
