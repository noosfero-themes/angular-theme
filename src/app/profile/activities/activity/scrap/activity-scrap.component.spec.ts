import { NgPipesModule } from "ngx-pipes";
import { By } from "@angular/platform-browser";
import { ProfileService } from "../../../../services/profile.service";
import { TranslateModule } from "@ngx-translate/core";
import { RouterTestingModule } from "@angular/router/testing";
import { TestBed, ComponentFixture } from "@angular/core/testing";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import * as helpers from "../../../../../spec/helpers";
import { FormsModule } from "@angular/forms";
import { ActivityScrapComponent } from "./activity-scrap.component";
import { DateFormatPipe } from "../../../../shared/pipes/date-format.pipe";
import { MomentModule } from "angular2-moment";
import { ScrapService } from "../../../../services/scrap.service";
import { Scrap } from "../../../../models/scrap.model";

describe("Components", () => {
  describe("ActivityScrapComponent", () => {
    const mocks = helpers.getMocks();
    let fixture: ComponentFixture<ActivityScrapComponent>;
    let component: ActivityScrapComponent;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterTestingModule,
          TranslateModule.forRoot(),
          FormsModule,
          NgPipesModule,
          MomentModule
        ],
        declarations: [ActivityScrapComponent, DateFormatPipe],
        providers: [
          { provide: ProfileService, useValue: mocks.profileService },
          { provide: ScrapService, useValue: mocks.scrapService },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      });
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(ActivityScrapComponent);
      component = fixture.componentInstance;
      component.scrap =  new Scrap();
      fixture.detectChanges();
    });

    it("render a noosfero activity tag for each activity", () => {
      expect(
        fixture.debugElement.queryAll(By.css(".activity-scrap")).length
      ).toEqual(1);
    });
  });
});
