import { Component, Input, OnInit } from "@angular/core";
import { Scrap } from "../../../../models/scrap.model";
import * as _ from "lodash";
import { ProfileService } from "../../../../services";
import { Profile } from "../../../../models/profile.model";
import { ScrapService } from "../../../../services/scrap.service";

@Component({
  selector: "noosfero-activity-scrap",
  templateUrl: "./activity-scrap.html",
  styleUrls: ["./activity-scrap.scss"]
})
export class ActivityScrapComponent implements OnInit {
  @Input() scrap: Scrap;
  profile: Profile;
  reply: Scrap;
  replies: Scrap[];
  MAX_CONTENT_SIZE = 415;
  showComments = false;

  constructor(
    private profileService: ProfileService,
    private scrapService: ScrapService
  ) {
    this.profile = profileService.getCurrentProfile();
    this.replies = [];
  }

  ngOnInit() {
    if (!_.isNil(this.scrap["user"])) {
      this.scrap.sender = this.scrap["user"];
    }
    if (!_.isNil(this.scrap["target"])) {
      this.scrap.receiver = this.scrap["target"];
    }

    if (!_.isNil(this.scrap["comments_count"])) {
      this.scrap.replies_count = this.scrap["comments_count"];
    }

    if (!_.isNil(this.scrap.replies_count) && this.scrap.replies_count > 0) {
      this.showComments = true;
    }
  }

  toggleReplayForm() {
    if (_.isNil(this.reply)) {
      this.reply = new Scrap();
    } else {
      this.hideReply();
    }
  }

  hasParent(): boolean {
    return !_.isNil(this.scrap.scrap_id);
  }

  displayComments() {
    this.scrapService
      .getReplies(this.profile, this.scrap)
      .subscribe((scraps: Scrap[]) => {
        this.replies = scraps;
        this.showComments = false;
        this.scrap.replies_count = this.replies.length;
      });
  }

  hideComments() {
    this.replies = [];
    this.showComments = true;
  }

  showReplyForm(): boolean {
    return !_.isNil(this.reply);
  }

  contentLeft(): number {
    let contentLeft = this.MAX_CONTENT_SIZE;
    if (!_.isNil(this.reply) && !_.isNil(this.reply.content)) {
      contentLeft = contentLeft - this.reply.content.length;
    }
    return contentLeft;
  }

  saveReply(scrap: Scrap) {
    scrap.scrap_id = this.scrap.id;
    this.scrapService
      .post(this.profile, scrap)
      .subscribe((localScrap: Scrap) => {
        this.displayComments();
        this.hideReply();
      });
  }

  private hideReply() {
    this.reply = null;
  }
}
