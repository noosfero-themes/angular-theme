import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { ProfileService } from "../../services/profile.service";
import { Router } from "@angular/router";

import { Profile } from "../../models/profile.model";
import { Activity } from "../../models/activity.model";
import {
  trigger,
  state,
  style,
  transition,
  animate
} from "@angular/animations";
import { Scrap } from "../../models/scrap.model";
import * as _ from "lodash";
import { ScrapService } from "../../services/scrap.service";

@Component({
  selector: "noosfero-activities",
  templateUrl: "./activities.html",
  styleUrls: ["./activities.scss"],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger("fadeInOut", [
      state(
        "void",
        style({
          opacity: 0
        })
      ),
      transition("void <=> *", animate(1000))
    ])
  ]
})
export class ActivitiesComponent implements OnInit {
  activities: any;
  page = 1;
  profile: Profile;
  hasActivities = false;
  loading = false;
  constructor(
    private profileService: ProfileService,
    private router: Router,
    private scrapService: ScrapService
  ) {}

  ngOnInit() {
    this.profile = this.profileService.getCurrentProfile();

    this.profileService.profileChangeEvent.subscribe(profile => {
      this.profile = profile;
    });

    this.loading = true;
    this.profileService
      .getActivities(this.profile, { page: this.page })
      .subscribe(
        (activities: Activity[]) => {
          this.activities = activities;
          if (this.activities.length > 0) {
            this.hasActivities = true;
          }
          this.loading = false;
        },
        (response: any) => {
          if (response.status === 403) {
            this.router.navigate([
              "/profile",
              this.profile.identifier,
              "about"
            ]);
          }
          this.loading = false;
        }
      );
    // this.scrapService.scrapCreated.subscribe((scrap: Scrap) => {
    //   this.activities = [scrap].concat(this.activities);
    // });
  }

  viewMore() {
    this.page++;
    this.loading = true;
    this.profileService
      .getActivities(this.profile, { page: this.page })
      .subscribe((activities: Activity[]) => {
        this.loading = false;
        this.hasActivities = activities.length > 0;
        activities.forEach((value, key) => {
          this.activities.push(value);
        });
      });
  }

  isCommunity(): boolean {
    return this.profile.type === "Community";
  }
}
