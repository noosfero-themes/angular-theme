import { NgPipesModule } from 'ngx-pipes';
import { By } from '@angular/platform-browser';
import { ProfileService } from '../../services/profile.service';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ActivitiesComponent } from './activities.component';
import * as helpers from '../../../spec/helpers';
import { Observable } from 'rxjs/Observable';
import { ScrapService } from '../../services/scrap.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


describe("Components", () => {
  describe("Activities Component", () => {
    const mocks = helpers.getMocks();
    let fixture: ComponentFixture<ActivitiesComponent>;
    let component: ActivitiesComponent;

    beforeEach(() => {
      spyOn(mocks.profileService, "getActivities").and.returnValue(Observable.of([{ name: "activity1", verb: "create_article" }, { name: "activity2", verb: "create_article" }]));
      TestBed.configureTestingModule({
        imports: [RouterTestingModule, TranslateModule.forRoot(), NgPipesModule, BrowserAnimationsModule],
        declarations: [ActivitiesComponent],
        providers: [
          { provide: ProfileService, useValue: mocks.profileService },
          { provide: ScrapService, useValue: mocks.scrapService },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      });
      fixture = TestBed.createComponent(ActivitiesComponent);
      component = fixture.componentInstance;
    });

    it("render a noosfero activity tag for each activity", () => {
      fixture.detectChanges();
      expect(fixture.debugElement.queryAll(By.css('noosfero-activity')).length).toEqual(2);
    });
  });
});
