import { ActivatedRoute, Router } from '@angular/router';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { ThemeService } from '../shared/services/theme.service';
import { DesignModeService } from '../shared/services/design-mode.service';
import { ProfileService } from '../services/profile.service';
import { NotificationService } from '../shared/services/notification.service';
import { AuthService } from '../services';
import { Profile } from '../models/profile.model';

@Component({
    selector: 'profile',
    templateUrl: './profile.html',
    styleUrls: ['./profile.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit {

    public profile: Profile;

    constructor(private profileService: ProfileService, private route: ActivatedRoute,
        private notificationService: NotificationService, private designModeService: DesignModeService,
        public authService: AuthService, private router: Router, private themeService: ThemeService) {

        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });
    }

    ngOnInit() {
        if (this.profile && this.themeService.verifyTheme(this.profile.theme)) return;

        this.designModeService.setInDesignMode(false);

        this.authService.loginSuccess.subscribe(() => {
            const currentProfile = this.profileService.getCurrentProfile();
            this.profileService.getByIdentifier(currentProfile.identifier, { optional_fields: ['boxes'] }).subscribe((profile: Profile) => {
                this.profile = profile;
            });
        });

        this.authService.logoutSuccess.subscribe(() => {
            this.profileService.getByIdentifier(this.profile.identifier, { optional_fields: ['boxes'] }).subscribe((profile: Profile) => {
                this.profile = profile;
                this.profile.permissions = undefined;
            });
        });

    }
}
