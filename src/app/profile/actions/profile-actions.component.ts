import {
  Component,
  Input,
  Inject,
  ViewEncapsulation,
  OnInit
} from "@angular/core";

import { ArticleService } from "../../services/article.service";
import { Profile } from "../../models/profile.model";
import { Article } from "../../models/article.model";

@Component({
  selector: "profile-actions",
  templateUrl: "./profile-actions.html",
  styleUrls: ["./profile-actions.scss"],
  encapsulation: ViewEncapsulation.None
})
export class ProfileActionsComponent implements OnInit {
  @Input() profile: Profile;
  blogs: Article[];

  constructor(private articleService: ArticleService) {
    this.blogs = [];
  }

  ngOnInit() {
    this.loadBlogs();
  }

  hasOneBlog(): boolean {
    return this.blogs ? this.blogs.length === 1 : false;
  }

  loadBlogs() {
    if (this.profile) {
      this.articleService
        .getByProfile(this.profile, { content_type: "Blog" })
        .subscribe(result => {
          this.blogs = result.body;
        });
    }
  }
}
