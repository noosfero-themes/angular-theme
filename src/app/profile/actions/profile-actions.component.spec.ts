import { TranslateModule } from '@ngx-translate/core';
import { TranslatorService } from '../../shared/services/translator.service';
import { ArticleService } from '../../services/article.service';
import { Input, Component } from '@angular/core';
import * as helpers from '../../../spec/helpers';
import { ProfileActionsComponent } from './profile-actions.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BsDropdownModule } from 'ngx-bootstrap';
import { Profile } from '../../models/profile.model';
import { Article } from '../../models/article.model';
import { Observable } from 'rxjs/Observable';

describe('Profile Actions Component', () => {
    const mocks = helpers.getMocks();
    let fixture: ComponentFixture<ProfileActionsComponent>;
    let component: ProfileActionsComponent;

    beforeEach(async(() => {
        spyOn(mocks.articleService, "getByProfile").and.callThrough();

        TestBed.configureTestingModule({
            imports: [BsDropdownModule.forRoot(), TranslateModule.forRoot()],
            declarations: [ProfileActionsComponent],
            providers: [
                { provide: ArticleService, useValue: mocks.articleService },
                { provide: TranslatorService, useValue: mocks.translatorService }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        });
        fixture = TestBed.createComponent(ProfileActionsComponent);
        component = fixture.componentInstance;
        component.profile = <Profile>{ id: 1, identifier: 'adminuser', type: "Person", permissions: ['allow_edit'] };
    }));

    it('renders content viewer actions directive', () => {
        expect(queryAll(".profile-menu").length).toEqual(1);
    });

    it("render the actions new item menu", () => {
        expect(queryAll("a[class|='btn btn-sm btn-primary']")[0]).not.toBeNull();
    });

    it("render no menu item actions by default", fakeAsync(() => {
        renderDynamicDropDownMenu();
        expect(queryAll(".profile-actions-item").length).toBe(0);
    }));

    it("render blog item action if has profile blog", fakeAsync(() => {
        TestBed.get(ArticleService).getByProfile = jasmine.createSpy('getByProfile').and.returnValue(Observable.of({ body: [<Article>{ id: 1, title: 'some title' }] }));

        renderDynamicDropDownMenu();
        expect(fixture.debugElement.queryAll(By.css(".profile-actions-item")).length).toBe(1);
    }));

    it("render all blogs item action if has profile more than one blog", fakeAsync(() => {
        TestBed.get(ArticleService).getByProfile = jasmine.createSpy('getByProfile').and.returnValue(Observable.of({ body: [<Article>{ id: 1, title: 'some title' }, <Article>{ id: 2, title: 'another title' }] }));

        renderDynamicDropDownMenu();
        expect(fixture.debugElement.queryAll(By.css(".profile-actions-item")).length).toBe(2);
    }));

    function queryAll(selector: string) {
        const compiled = fixture.debugElement;
        return compiled.queryAll(By.css(selector));
    }

    function renderDynamicDropDownMenu() {
        fixture.detectChanges();
        const toggleButton = fixture.nativeElement.querySelector('button');
        toggleButton.click();
        tick();
        fixture.detectChanges();
    }
});
