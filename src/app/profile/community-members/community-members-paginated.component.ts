import { Component, Inject, Input, Output, EventEmitter, ViewEncapsulation, OnInit } from '@angular/core';
import { DisplayStyles } from '../profile-list/profile-list.component';
import { Person } from '../../models/person.model';
import { Profile } from '../../models/profile.model';

@Component({
    selector: "noosfero-community-members-paginated",
    templateUrl: './community-members-paginated.html',
    styleUrls: ['./community-members-paginated.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CommunityMembersPaginatedComponent implements OnInit {

    @Input() private members: Person[];
    private currentPage: number;
    @Input() private itensPerPage: number;
    @Input() private totalItems: number;
    @Input() profile: Profile;
    @Output() pageChanged = new EventEmitter();

    private displayStyle: string = DisplayStyles.card;

    constructor() { }

    changePage($event) {
        this.pageChanged.emit($event);
    }

    ngOnInit() {
        this.currentPage = 1;
    }

    getDisplayStyle() {
        return this.displayStyle;
    }

}
