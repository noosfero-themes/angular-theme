import { Router, ActivatedRoute } from '@angular/router';
import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { ArticleService } from '../services/article.service';
import * as _ from "lodash";
import { TabsetComponent } from 'ngx-bootstrap';

@Component({
  selector: 'search',
  templateUrl: './search.html',
  styleUrls: ['./search.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SearchComponent implements OnInit {

  searchType: string;
  query: string;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  date = new Date();
  firstLoad = true;

  SEARCH_TYPE = {
    ARTICLES: 'Articles',
    PEOPLE: 'People',
    COMMUNITIES: 'Communities'
  }

  constructor(protected articleService: ArticleService, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data) => {
      this.searchType = data['search_type'];
      this.selectTab();
      this.firstLoad = false;
    });

    this.activatedRoute.queryParams.subscribe(getParams => {
      this.query = getParams['query'];
    });
  }


  selectTab() {
    switch (this.searchType) {
      case this.SEARCH_TYPE.ARTICLES:
        this.staticTabs.tabs[0].active = true;
        break;
      case this.SEARCH_TYPE.PEOPLE:
        this.staticTabs.tabs[1].active = true;
        break;
      case this.SEARCH_TYPE.COMMUNITIES:
        this.staticTabs.tabs[2].active = true;
        break;
      default:
        this.staticTabs.tabs[0].active = true;
    }
  }

  changeTab(searchType) {
    this.query = this.activatedRoute.snapshot.queryParams['search'];
    this.searchType = searchType;
    if (!this.firstLoad) {
      this.firstLoad = true;
      switch (searchType) {
        case "People":
          this.router.navigate(['search', 'people'], { queryParams: { search: this.query } });
          break;
        case "Communities":
          this.router.navigate(['search', 'communities'], { queryParams: { search: this.query } });
          break;
        default:
          this.router.navigate(['search', 'articles'], { queryParams: { search: this.query } });
      }
    } else {
      this.firstLoad = false;
    }

  }

  isSearchArticles() {
    return this.searchType === 'Articles';
  }

  isSearchPeople() {
    return this.searchType === 'People';
  }

  isSearchCommunity() {
    return this.searchType === 'Communities';
  }

}
