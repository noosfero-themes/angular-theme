import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.html',
  styleUrls: ['./search-form.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SearchFormComponent implements OnInit {

  query: string;

  constructor(private route: ActivatedRoute, private router: Router) {
    router.events
    .filter(event => event instanceof NavigationEnd)
    .subscribe((event: NavigationEnd) => {
      if (event.url.match('/search')) {
        const match = event.url.match("search=[^&]*")
        this.query = match ? decodeURI(match[0].split('=')[1]) : '';
      } else {
        this.query = '';
      }
    });
  }

  ngOnInit() {
    this.query = this.route.snapshot.queryParams['search'];
  }

  search() {
    this.router.navigate(['/search'], { queryParams: { search: this.query } });
  }
}
