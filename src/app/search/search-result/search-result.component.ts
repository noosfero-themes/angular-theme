import { Router, ActivatedRoute } from '@angular/router';
import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/article.model';
import * as _ from "lodash";
import { DisplayStyles } from '../../profile/profile-list/profile-list.component';
import { PersonService } from '../../services/person.service';
import { Profile } from '../../models/profile.model';
import { CommunityService } from '../../services/community.service';
import { Location } from '@angular/common';


@Component({
  selector: 'search-result',
  templateUrl: './search-result.html',
  styleUrls: ['./search-result.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SearchResultComponent implements OnInit {

  articles: Article[] = [];
  profiles: Profile[] = [];
  tag: string;
  content_type: string;
  totalResults = 0;
  perPage = 20;
  page = 0;
  loading = false;
  firstLoad = true;
  @Input() searchType;
  @Input() profile;
  @Input() query: string;
  fromStartDate: any;
  untilStartDate: any;
  fromEndDate: any;
  untilEndDate: any;
  filterDate = false;
  private displayStyle: string = DisplayStyles.card;


  constructor(protected articleService: ArticleService,
    private router: Router,
    private location: Location,
    private personService: PersonService,
    private communityService: CommunityService,
    private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(getParams => {

      this.query = getParams['search'];
      this.tag = getParams['tag'];

      if (getParams['from_start_date']) {
        this.fromStartDate = new Date(getParams['from_start_date']);
        this.filterDate = true;
      }

      if (getParams['until_start_date']) {
        this.untilStartDate = new Date(getParams['until_start_date']);
      }

      if (getParams['from_end_date']) {
        this.fromEndDate = new Date(getParams['from_end_date']);
      }

      if (getParams['until_end_date']) {
        this.untilEndDate = new Date(getParams['until_end_date']);
        this.filterDate = true;
      }
      this.content_type = getParams['content_type'] || '';
      this.perPage = getParams['per_page'] || this.perPage;
      this.page = +getParams['page'] || 0;
      if (this.firstLoad) {
        this.loadPage();
      }
    });

  }

  pageChange(event: any) {
    if (this.loading) {
      this.firstLoad = false;
      return;
    }
    if (!_.isNil(event)) {
      this.page = +event.page;
    }
    const filter = this.makeFilter();
    const base = this.getBaseUrl(this.location.path());
    this.router.navigate([base], { queryParams: filter });
  }

  makeFilter(): any {
    const filter = {
      per_page: this.perPage,
      page: this.page,
      order: (this.searchType === 'Articles' ? 'created_at DESC' : 'name ASC'),
      search_type: this.searchType
    };

    if (!_.isNil(this.fromStartDate)) {
      filter['from_start_date'] = this.fromStartDate;
    }

    if (!_.isNil(this.untilStartDate)) {
      filter['until_start_date'] = this.untilStartDate;
    }

    if (!_.isNil(this.fromEndDate)) {
      filter['from_end_date'] = this.fromEndDate
    }

    if (!_.isNil(this.untilEndDate)) {
      filter['until_end_date'] = this.untilEndDate
    }


    if (!_.isNil(this.query)) {
      filter['search'] = this.query;
    }

    if (!_.isNil(this.tag)) {
      filter['tag'] = this.tag;
    }

    if (!_.isNil(this.content_type) && !_.isEmpty(this.content_type)) {
      filter['content_type'] = this.content_type;
    }

    return filter;
  }

  search() {
    this.pageChange(null);
  }

  loadPage() {
    switch (this.searchType) {
      case "Articles":
        this.searchArticles();
        break;
      case "People":
        this.searchPeople();
        break;
      case "Communities":
        this.searchCommunities();
        break;
      default:
        this.searchArticles();
    }
  }

  articleRouterLink(article: Article): string[] {
    let link = ['/', article.profile.identifier];
    link = link.concat(article.path.split('/'));

    return link;
  }

  searchArticles() {
    this.loading = true;
    this.profiles = [];
    if (_.isNil(this.profile)) {
      this.articleService.search(this.makeFilter()).subscribe((result: any) => {
        this.totalResults = <number>(<any>result.headers).get("total");
        this.articles = result.body;
        this.loading = false;
      });
    } else {
      this.articleService.getByProfile(this.profile, this.makeFilter()).subscribe((result: any) => {
        this.totalResults = <number>(<any>result.headers).get("total");
        this.articles = result.body;
        this.loading = false;
      });
    }
  }

  searchPeople() {
    this.loading = true;
    this.articles = [];
    this.content_type = null;
    this.personService.search(this.makeFilter()).subscribe((result: any) => {
      this.totalResults = <number>(<any>result.headers).get("total");
      this.profiles = result.body;
      this.loading = false;
    });
  }

  searchCommunities() {
    this.loading = true;
    this.articles = [];
    this.content_type = null;
    this.communityService.search(this.makeFilter()).subscribe((result: any) => {
      this.totalResults = <number>(<any>result.headers).get("total");
      this.profiles = result.body;
      this.loading = false;
    });
  }

  hasBody(content: any): boolean {
    let hasContent = true;
    if (_.isNil(content)) {
      hasContent = false;
    } else if (_.isEmpty(hasContent)) {
      hasContent = false;
    }

    return hasContent;
  }

  getBaseUrl(url: string): string {
    return url.replace(/\?.*/, '');
  }

  isSearchArticles() {
    return this.searchType === 'Articles';
  }

  getDisplayStyle() {
    return this.displayStyle;
  }

  displayPagination(): boolean {
    return (this.totalResults > 0) && !this.loading;

  }

}
