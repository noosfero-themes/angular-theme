import { Router } from '@angular/router';
import { Inject, Input, Component, ChangeDetectorRef, ViewEncapsulation, OnDestroy, OnInit, DoCheck } from '@angular/core';
import { EventsHubService } from '../../shared/services/events-hub.service';
import { NoosferoKnownEvents } from '../../known-events';
import { BlockService } from '../../services/block.service';
import { ProfileService } from '../../services/profile.service';
import { EnvironmentService } from '../../services/environment.service';
import { NotificationService } from '../../shared/services/notification.service';
import { DesignModeService } from '../../shared/services/design-mode.service';
import * as _ from "lodash";
import { Profile } from '../../models/profile.model';
import { Environment } from '../../models/environment.model';
import { Block } from '../../models/block.model';
import { Box } from '../../models/box.model';

@Component({
  selector: "context-bar",
  templateUrl: './context-bar.html',
  styleUrls: ['./context-bar.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ContextBarComponent implements OnInit, OnDestroy, DoCheck {

  @Input() owner: Profile | Environment;
  @Input() permissionAction = 'allow_edit';

  blocksChanged: Block[];
  designModeOn = false;
  originalLayout: string;
  originalCustomHeader: string;
  originalCustomFooter: string;
  destroyed = false;

  constructor(private ref: ChangeDetectorRef,
    private router: Router,
    private eventsHubService: EventsHubService,
    private blockService: BlockService,
    private notificationService: NotificationService,
    private designModeService: DesignModeService,
    private profileService: ProfileService,
    private environmentService: EnvironmentService) {
  }

  ngOnInit() {
    if (this.isProfile()) {
      this.originalCustomHeader = (<Profile>this.owner).custom_header;
      this.originalCustomFooter = (<Profile>this.owner).custom_footer;
    }
    this.originalLayout = this.owner.layout_template;
    this.blocksChanged = [];

    this.blockService.blockChanged.subscribe((block: Block) => {
      this.pushChangedBlock(block);
    });
    this.designModeService.onToggle.subscribe((designModeOn: boolean) => {
      this.designModeOn = designModeOn;
    });
    this.designModeOn = this.designModeService.isInDesignMode();
  }

  private blocksAreEquals(block: Block, anotherBlock: Block): boolean {

    if (!block || !anotherBlock) return false

    let isEqual = false;
    isEqual = (block.id === anotherBlock.id) ? true : false;
    isEqual = (block._temp_id === anotherBlock._temp_id) ? true : false;
    isEqual = (block.box_id === anotherBlock.box_id) ? isEqual : false;

    return isEqual;
  }

  pushChangedBlock(block: Block) {
    let blockChanged = null;
    if (_.isNil(block.id)) {
      blockChanged = _.find(this.blocksChanged, { '_temp_id': block._temp_id, 'box_id': block.box_id });
    } else {
      blockChanged = _.find(this.blocksChanged, { 'id': block.id, 'box_id': block.box_id });

    }

    if (this.blocksAreEquals(blockChanged, block)) {
      blockChanged = Object.assign(blockChanged, block)

      if (block._add && !_.isNil(blockChanged._destroy)) {
        delete blockChanged._destroy;
      }
    } else {
      this.blocksChanged.push(block);
    }

    if (!this.destroyed) this.ref.detectChanges();
  }

  ngOnDestroy() {
    this.destroyed = true;
  }

  ngDoCheck() {
    this.designModeService.hasChanges = this.hasChanges();
  }

  private getOwnerService(): any {
    if (this.isProfile()) {
      return this.profileService;
    } else {
      return this.environmentService;
    }
  }

  apply() {
    if (!this.isLayoutTemplateChanged() && !this.hasBlockChanges()) return Promise.resolve();
    this.applyChanges();
  }

  discard() {
    this.getOwnerService().getBoxes(this.owner).subscribe((boxes: Box[]) => {
      this.owner.boxes = boxes;
      this.blocksChanged = [];
      this.owner.layout_template = this.originalLayout;
      this.notificationService.info({ title: "contextbar.edition.discard.success.title", message: "contextbar.edition.discard.success.message" });
      this.designModeService.setInDesignMode(false);
    });
  }

  private parseBlockFieldsToSave(block: Block): Block {

    const updatableFields = ['id', '_add', '_destroy', 'box_id', 'api_content', 'type', 'title', 'position']

    block.api_content = block.api_content ? Object.assign(block.settings, block.api_content) : block.settings;

    if (block._add) {
      block.id = (block.box_origin_id !== block.box_id) ? null : block.id
      delete block._add;
      delete block._destroy;
    }

    return <Block>_.pick(block, updatableFields);

  }

  applyChanges() {
    const boxesHolder = { id: this.owner.id };

    if (this.hasBlockChanges()) {
      let copyBlocksChanged = _.cloneDeep(this.blocksChanged)

      copyBlocksChanged = _.reject(copyBlocksChanged, function (block) {
        return block._destroy && (_.isNil(block.id) || block._add);
      });

      const parsedCopyBlocksChanged = []
      _.forEach(copyBlocksChanged, (b) => {
        parsedCopyBlocksChanged.push(this.parseBlockFieldsToSave(b));
      });

      const groupedBoxesChanged = _.groupBy(parsedCopyBlocksChanged, (block) => {
        return block.box_id;
      });

      const boxes = [];
      Object.keys(groupedBoxesChanged).forEach(function (key) {
        boxes.push({ id: +key, blocks_attributes: groupedBoxesChanged[key] });
      });

      boxesHolder['boxes_attributes'] = boxes;
    }
    if (this.isLayoutTemplateChanged()) boxesHolder['layout_template'] = this.owner.layout_template;
    this.getOwnerService().update(boxesHolder, { optional_fields: ['boxes'] }).subscribe((holder) => {
      this.blocksChanged = [];
      this.originalLayout = this.owner.layout_template;
      let remoteBlocks = [];
      holder.boxes.forEach((box) => {
        remoteBlocks = _.concat(remoteBlocks, box.blocks);
      });

      remoteBlocks.forEach((block) => {
        block._force_update = true;
      });

      this.blockService.blocksSaved.next(remoteBlocks);
      this.notificationService.success({ title: "contextbar.edition.apply.success.title", message: "contextbar.edition.apply.success.message" });
      this.designModeService.setInDesignMode(false);
    })
  }

  hasChanges() {
    return this.hasBlockChanges() || this.isLayoutTemplateChanged();
  }

  hasBlockChanges() {
    return this.blocksChanged.length > 0;
  }

  isLayoutTemplateChanged() {
    return this.owner && this.originalLayout !== this.owner.layout_template;
  }

  isProfile() {
    let type = '';
    if (this.owner !== undefined) {
      type = (<any>this.owner)['type'];
    }
    return type === "Community" || type === "Person";
  }

}
