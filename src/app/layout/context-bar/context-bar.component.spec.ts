import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationService } from '../../shared/services/notification.service';
import { TranslatorService } from '../../shared/services/translator.service';
import { EventsHubService } from '../../shared/services/events-hub.service';
import { DesignModeService } from '../../shared/services/design-mode.service';
import { BlockService } from '../../services/block.service';
import { EnvironmentService } from '../../services/environment.service';
import { ProfileService } from '../../services/profile.service';
import { Component } from '@angular/core';
import { ContextBarComponent } from './context-bar.component';
import { PermissionNg2Directive } from '../../shared/components/permission/permission.ng2.directive';
import * as helpers from '../../../spec/helpers';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DragulaModule } from 'ng2-dragula';
import { Profile } from '../../models/profile.model';
import { Block } from '../../models/block.model';
import { Environment } from '../../models/environment.model';
import { Observable } from 'rxjs/Observable';

describe("Context Bar Component", () => {
  const mocks = helpers.getMocks();
  let fixture: ComponentFixture<ContextBarComponent>;
  let component: ContextBarComponent;

  beforeEach(async(() => {
    spyOn(mocks.profileService, 'update').and.callThrough();
    spyOn(mocks.environmentService, 'update').and.callThrough();
    spyOn(mocks.profileService, 'getBoxes').and.callThrough();
    spyOn(mocks.notificationService, 'success').and.callThrough();

    TestBed.configureTestingModule({
      declarations: [ContextBarComponent, PermissionNg2Directive],
      providers: [
        { provide: EventsHubService, useValue: mocks.eventsHubService },
        { provide: BlockService, useValue: mocks.blockService },
        { provide: NotificationService, useValue: mocks.notificationService },
        { provide: DesignModeService, useValue: mocks.designModeService },
        { provide: ProfileService, useValue: mocks.profileService },
        { provide: EnvironmentService, useValue: mocks.environmentService },
        { provide: TranslatorService, useValue: mocks.translatorService }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule, TranslateModule.forRoot(), DragulaModule]
    });
    fixture = TestBed.createComponent(ContextBarComponent);
    component = fixture.componentInstance;
    component.originalLayout = 'default';
    component.blocksChanged = [];
    component.owner = <Profile>{
      id: 1,
      identifier: 'profile-name',
      type: 'Person',
      layout_template: 'default',
      permissions: ['allow_edit'],
      boxes: [{ id: 6, blocks: [{ id: 5, box_id: 6 }] }]
    };
    fixture.detectChanges();
  }));

  it("render template context-bar", () => {
    expect(all('#context-bar').length).toEqual(1);
  });

  it("not call block service to apply blocks changes when no changes exists", () => {
    component.blocksChanged = <Block[]>[];
    component.apply();
    expect(mocks.profileService.update).not.toHaveBeenCalled();
  });

  it("call block service to apply blocks changes", () => {
    component.blocksChanged = <Block[]>[{ id: 1, box_id: 6 }];
    component.applyChanges();
    expect(TestBed.get(ProfileService).update).toHaveBeenCalled();
  });

  it("return false when there is no blocks to be updated", () => {
    expect(component.hasBlockChanges()).toBeFalsy();
  });

  it("return true when exists blocks to be updated", () => {
    component.blocksChanged = <Block[]>[{ id: 1, box_id: 6 }];
    expect(component.hasBlockChanges()).toBeTruthy();
  });

  it("add block to blocksChanged when receive an event", () => {
    const blockChanged = <Block>{ id: 2, title: 'changed' };
    mocks.blockService.blockChanged.next(blockChanged);
    expect(component.blocksChanged).toEqual([blockChanged]);
  });

  it("replace block to blocksChanged when receive an event", () => {
    let blockChanged = <Block>{ id: 2, box_id: 1, title: 'changed' };
    mocks.blockService.blockChanged.next(blockChanged);
    blockChanged = <Block>{ id: 2, box_id: 1, title: 'changed again' };
    mocks.blockService.blockChanged.next(blockChanged);
    expect(component.blocksChanged).toEqual([blockChanged]);
  });

  it("call profile service to update template when apply", () => {
    component.owner.layout_template = "leftbar";
    component.applyChanges();
    expect(mocks.profileService.update).toHaveBeenCalledWith({ id: 1, layout_template: "leftbar" }, { optional_fields: ['boxes'] });
  });

  it("call environment service when owner is an environment", () => {
    component.owner = <Environment>{ id: 2, layout_template: 'default', boxes: [] };
    component.owner.layout_template = "rightbar";
    component.applyChanges();
    expect(mocks.environmentService.update).toHaveBeenCalledWith({ id: 2, layout_template: "rightbar" }, { optional_fields: ['boxes'] });
  });

  it("load boxes when discard changes", () => {
    component.discard();
    expect(mocks.profileService.getBoxes).toHaveBeenCalled();
  });

  it("call notification success when apply changes", fakeAsync(() => {
    component.blocksChanged = <any>[{ id: 5, _destroy: true, box_id: 6 }];
    component.apply();
    fixture.detectChanges();
    tick();
    expect(component['notificationService'].success).toHaveBeenCalledWith({ title: "contextbar.edition.apply.success.title", message: "contextbar.edition.apply.success.message" });
  }));

  it("disable edit mode when changes were applied successfully", fakeAsync(() => {
    component.blocksChanged = <any>[{ id: 5, _destroy: true, box_id: 6 }];
    TestBed.get(DesignModeService).setInDesignMode(true);
    component.apply();
    tick();
    expect(TestBed.get(DesignModeService).isInDesignMode()).toBeFalsy();
  }));

  it("render template context-bar if block is marked for removal", () => {
    component.blocksChanged = <any>[{ id: 5, _destroy: true, box_id: 6 }];
    fixture.detectChanges();
    expect(all('#context-bar .apply').length).toEqual(1);
  });

  it("call ProfileService.update if apply button is pressed", () => {
    component.blocksChanged = <any>[{ id: 5, _destroy: true, box_id: 6, settings: {} }];
    component.applyChanges();
    expect(mocks.profileService.update).toHaveBeenCalledWith({ id: 1, boxes_attributes: [{ id: 6, blocks_attributes: [{ id: 5, _destroy: true, box_id: 6, api_content: {} }] }] }, { optional_fields: ['boxes'] });
  });

  it("call mocks.EnvironmentService.update if apply button is pressed", () => {
    component.owner.type = "Environment";
    component.blocksChanged = <any>[{ id: 5, _destroy: true, box_id: 6, settings: {} }];
    component.applyChanges();
    expect(mocks.environmentService.update).toHaveBeenCalledWith({ id: 1, boxes_attributes: [{ id: 6, blocks_attributes: [{ id: 5, _destroy: true, box_id: 6, api_content: {} }] }] }, { optional_fields: ['boxes'] });
  });

  function all(selector: string) {
    return fixture.debugElement.queryAll(By.css(selector));
  }
});
