import { Component, Inject, Input, ViewEncapsulation } from '@angular/core';
import { DesignModeService } from '../../shared/services/design-mode.service';
import { AuthService, AuthEvents } from '../../services';
import { HttpLoadingService } from '../../shared/services/http-loading.service';

@Component({
  selector: 'design-toggler',
  templateUrl: './design-mode-toggler.html',
  styleUrls: ['./design-mode-toggler.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DesignModeTogglerComponent {

  private _inDesignMode = false;
  isLoading = true;

  constructor(
    private designModeService: DesignModeService,
    private httpLoadingService: HttpLoadingService,
    private authService: AuthService) {
    this.authService.logoutSuccess.subscribe(() => {
      this.designModeService.destroy();
    });
    this.httpLoadingService.isLoadingChanged.subscribe((value) => {
      this.isLoading = !value;
    });
  }

  get inDesignMode(): boolean {
    return this.designModeService.isInDesignMode();
  };

  set inDesignMode(value: boolean) {
    this.designModeService.setInDesignMode(value);
  };

  togleDesignMode() {
    const value = this.designModeService.isInDesignMode();
    this.designModeService.setInDesignMode(!value);
  }
}
