import { Inject, Input, Component, OnInit, OnDestroy } from '@angular/core';
import { DesignModeService } from '../../shared/services/design-mode.service';
import { EventsHubService } from '../../shared/services/events-hub.service';
import { BlockService } from '../../services/block.service';
import { DragulaService } from 'ng2-dragula';
import { NoosferoKnownEvents } from '../../known-events';
import * as _ from 'lodash';
import { Box } from '../../models/box.model';
import { Profile } from '../../models/profile.model';
import { Environment } from '../../models/environment.model';
import { Block } from '../../models/block.model';

@Component({
  selector: "noosfero-box",
  templateUrl: './box.html',
  styleUrls: ['./box.scss'],
})
export class BoxComponent implements OnInit {

  @Input() box: Box;
  @Input() boxes: Box[];
  @Input() owner: Profile | Environment;
  @Input() designMode: boolean;
  @Input() column: any;
  @Input() startIndex: number;

  orderedBlocks: Block[] = [];
  bagId: any;
  draggerContainerName = 'draggerContainerNameId';
  dragulaOptions = {};

  removedBlocks = [];

  constructor(private eventsHubService: EventsHubService,
    private blockService: BlockService,
    private dragulaService: DragulaService) {
  }

  ngOnInit() {

    if (_.isNil(this.box)) {
      return null;
    }

    this.bagId = 'box-bag-' + this.box.id;

    this.dragulaOptions = {
      // FIXME remove this logs
      // revertOnSpill: true,
      moves: function (el, container, handle) {
        return handle.className === 'dragula-handle';
      }
    };

    this.box.blocks = _.sortBy(this.box.blocks, ['position'])

    this.dragulaService.drag.subscribe((value) => {
      this.blockService.hideContent.next(true)

    });

    this.dragulaService.drop.subscribe((value) => {

      const [bagName, el, target, source, elTarget] = value;

      const allBlocks = this.getAllBlocks();

      let changedBlock = null;

      if (_.filter(allBlocks, { 'id': +el.dataset.id }).length > 1) {
        changedBlock = _.find(allBlocks, function (b) {
          return (((b.id === +el.dataset.id) && _.isNil(b._destroy)))
        });
      } else {
        changedBlock = _.find(allBlocks, { 'id': +el.dataset.id });
      }

      if (changedBlock) {
        changedBlock.position = (elTarget) ? +elTarget.dataset.index + 1 : this.box.blocks.length + 1
      }

      let hasChanges = false;
      if (changedBlock && (target === source)) {
        if (changedBlock.box_id === this.box.id) {
          this.reorderBlocks(this.box, changedBlock);
          hasChanges = true;
        }

      } else if (changedBlock && (target.dataset.id === this.bagId)) {
        this.addBlockInBox(this.box, changedBlock);
        hasChanges = true;
      } else if (changedBlock && (source.dataset.id === this.bagId)) {
        const targetID = target.dataset.id.replace('box-bag-', '')
        this.removeBlock(changedBlock, +targetID);
        hasChanges = true;
      }

      if (hasChanges) {
        const allChanges = _.concat(this.box.blocks, this.removedBlocks)
        allChanges.forEach((block) => {
          this.blockService.blockChanged.next(block);
        });
      }

    });

    this.dragulaService.dragend.subscribe((value) => {
      this.blockService.hideContent.next(false)
    });

  }

  addBlockInBox(box: Box, newBlock: Block) {

    if (_.find(box.blocks, { 'id': newBlock.id })) {
      return null
    }

    const blockCopy = _.cloneDeep(newBlock);

    blockCopy.box_id = box.id;
    blockCopy._add = true
    if (_.isNil(newBlock.box_origin_id)) {
      blockCopy.box_origin_id = newBlock.box_id;
    }

    this.removedBlocks = _.reject(this.removedBlocks, { 'id': blockCopy.id });

    this.reorderBlocks(box, blockCopy);

  }

  getBlocksFromBoxesById(blockId: number): Block[] {

    const blockCopy = Object.assign([], _.filter(this.getAllBlocks(), { 'id': blockId }));

    return blockCopy;
  }

  getAllBlocks(): Block[] {
    let allBlocks = []
    this.boxes.forEach((box) => {
      allBlocks = _.concat(allBlocks, box.blocks);
    });

    return allBlocks;
  }

  removeBlock(removedBlock: Block, target) {
    const orderedBlocks = [];

    const otherBlocks = this.getBlocksFromBoxesById(removedBlock.id)
    if (target && (otherBlocks.length !== 2)) {
      this.addBlockInBox(this.getBoxById(target), removedBlock)
    }

    this.removedBlocks.push({ id: removedBlock.id, box_id: this.box.id, _destroy: true });
    this.box.blocks = _.reject(this.box.blocks, { 'id': removedBlock.id });


    this.reorderBlocks(this.box);
  }

  reorderBlocks(box: Box, fixedBlock?: Block) {
    const orderedBlocks = [];
    let position = 1


    box.blocks.forEach((block) => {

      if (!_.isNil(fixedBlock) && (position === fixedBlock.position)) {
        if (this.shouldAddBlockInBox(box, fixedBlock)) {
          orderedBlocks.push(fixedBlock);
        }
        position += 1;
      }

      if (_.isNil(fixedBlock) || (block.id !== fixedBlock.id) || (block._temp_id !== fixedBlock._temp_id)) {
        block.position = position;
        position += 1;
      }
      orderedBlocks.push(block);
    });
    box.blocks = orderedBlocks;

    if (this.shouldAddBlockInBox(box, fixedBlock)) {
      box.blocks.push(fixedBlock);
    }

  }

  private shouldAddBlockInBox(box: Box, block: Block): boolean {
    let shouldAddBlockInBox = false;
    shouldAddBlockInBox = _.isNil(block) ? false : true;
    shouldAddBlockInBox = shouldAddBlockInBox && !_.includes(box.blocks, block);
    return shouldAddBlockInBox;
  }


  addBlock(block: Block) {
    const valor = Math.random().toString(36).substring(2, 15);
    block._temp_id = valor
    block.box_id = this.box.id;
    block.position = 1;
    this.box.blocks.unshift(block);

    this.reorderBlocks(this.box, block);
  }


  private getBoxById(boxId: number): Box {
    return _.find(this.boxes, { 'id': boxId });
  }

  showButton(column: any) {
    return this.designMode && this.isNotParent(column);
  }

  isNotParent(column: any) {
    return column && column['subcolumns'] === undefined;
  }

}
