import { Input, Inject, Component, Output, EventEmitter } from '@angular/core';
import { Block } from '../../models/block.model';
import { Profile } from '../../models/profile.model';
import { Environment } from '../../models/environment.model';
import { blocks } from '.';

export interface BlockComponentInterface {

  block: Block;
  owner: Profile | Environment;
  designMode: boolean;
  canDisplayOnPreviewChanged: EventEmitter<boolean>;
  hideBlockChanged: EventEmitter<boolean>;

}

@Component({
  selector: 'noosfero-block-content',
  templateUrl: './block-content.html'

})
export class BlockContentComponent {

  @Input() block: Block;
  @Input() owner: Profile;
  @Input() designMode: boolean;
  @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  canDisplayOnPreviewChangedOnChild(canDisplayOnPreview: boolean) {
    this.canDisplayOnPreviewChanged.emit(canDisplayOnPreview);
  }

  hideBlockChangedOnChild(hideBlockChanged: boolean) {
    this.hideBlockChanged.emit(hideBlockChanged);
  }

}
