import { Input, Inject, Component, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { ArticleService } from '../../../services/article.service';
import { BlockService } from '../../../services/block.service';

import { StatisticsBlock } from '../../../models/statistics-block.model';
import { BlockComponentInterface } from '../block-content.component';


@Component({
  selector: "noosfero-statistics-block",
  templateUrl: './statistics-block.html',
  styleUrls: ['./statistics.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class StatisticsBlockComponent implements BlockComponentInterface {

  @Input() block: any;
  @Input() owner: any;
  @Input() designMode: boolean;
  @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(articleService: ArticleService, blockService: BlockService) {

    articleService.articleRemoved.subscribe(() => {
      blockService.getBlock(this.block).subscribe((block) => {
        this.block = <StatisticsBlock>block;
      });
    });

    articleService.articleCreated.subscribe(() => {
      blockService.getBlock(this.block).subscribe((block) => {
        this.block = <StatisticsBlock>block;
      });
    });
  }
}
