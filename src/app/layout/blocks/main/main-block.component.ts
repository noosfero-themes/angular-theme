import { Component, Input, EventEmitter, Output } from '@angular/core';
import { BlockComponentInterface } from '../block-content.component';

@Component({
  selector: 'noosfero-main-block',
  templateUrl: './main-block.html'
})
export class MainBlockComponent implements BlockComponentInterface {

  @Input() block: any;
  @Input() owner: any;
  @Input() designMode: boolean;
  @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

}
