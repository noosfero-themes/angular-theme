import { Component, Inject, Input, ViewEncapsulation, OnInit, EventEmitter, Output, OnChanges } from '@angular/core';
import { BlockService } from '../../../services/block.service';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';
import { BlockComponentInterface } from '../block-content.component';

@Component({
    selector: "noosfero-members-block",
    templateUrl: './members-block.html',
    styleUrls: ['./members-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class MembersBlockComponent implements OnInit, OnChanges, BlockComponentInterface {

    @Input() block: Block;
    @Input() owner: Profile;
    @Input() designMode: boolean;
    @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

    public loading = false;

    profiles: any = [];
    constructor(private blockService: BlockService) { }

    ngOnInit() {
        if (!this.loading) {
            this.getBlockContent()
        }
    }

    getBlockContent() {
        this.loading = true;

        if (this.block.api_content) {
            this.profiles = this.block.api_content['people'];
            this.loading = false;
        } else {
            this.blockService.getBlock(this.block).subscribe((block: Block) => {
                this.block.api_content = block.api_content;
                this.profiles = this.block.api_content.people;
                this.loading = false;
            });
        }

    }

    isEnvironment() {
        return this.owner && this.owner.type === "Environment";
    }

    ngOnChanges() {
        if (!this.loading) {
            this.getBlockContent()
        }
    }
}
