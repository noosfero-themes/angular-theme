import { DesignModeService } from '../../../shared/services/design-mode.service';
import { TranslateModule } from '@ngx-translate/core';
import { TranslatorService } from '../../../shared/services/translator.service';
import { PopoverModule } from 'ngx-bootstrap';
import { DragulaModule } from 'ng2-dragula';
import { ArticleService } from '../../../services/article.service';
import { MenuBlockComponent } from './menu-block.component';
import * as helpers from '../../../../spec/helpers';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { tick, fakeAsync, async, TestBed, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Profile } from '../../../models/profile.model';
import { Person } from '../../../models/person.model';
import { Article } from '../../../models/article.model';
import { MenuBlockLink } from '../../../models/menu-block-link.model';
import * as _ from "lodash";

describe("Components", () => {
  describe("Menu Block Component", () => {
    const mocks = helpers.getMocks();
    let fixture: ComponentFixture<MenuBlockComponent>;
    let component: MenuBlockComponent;
    const articles = [
      { name: 'article 1', path: 'article-1' },
      { name: 'article 2', path: 'article-2' }
    ];
    const articleService = jasmine.createSpyObj("ArticleService", ["getByProfile"]);
    articleService.getByProfile = jasmine.createSpy("getByProfile").and.returnValue(Promise.resolve({ headers: () => { }, data: articles }));

    const dragulaService = jasmine.createSpyObj("DragulaService", ["dropModel"]);
    dragulaService.dropModel = jasmine.createSpy("dropModel").and.returnValue(Promise.resolve({ headers: () => { } }));

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [FormsModule, DragulaModule, PopoverModule.forRoot(), TranslateModule.forRoot()],
        declarations: [MenuBlockComponent],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [
          { provide: ArticleService, useValue: articleService },
          { provide: DesignModeService, useValue: mocks.designModeService },
          { provide: TranslatorService, useValue: mocks.translatorService }
        ]
      });
      fixture = TestBed.createComponent(MenuBlockComponent);
      component = fixture.componentInstance;
      component.owner = <Profile>{ id: 1, name: 'profile-name', identifier: 'profile-name' };
      component.block = {
        id: 1,
        type: 'MenuBlock',
        api_content: {
          enabled_items: [
            { "title": "Activities", "controller": "profile", "action": "activities" },
            { "title": "About", "controller": "profile", "action": "about" }
          ],
          available_items: [
            { "title": "About", "controller": "profile", "action": "about" },
            { "title": "Communities", "controller": "memberships", "action": "index" },
            { "title": "People", "controller": "friends", "action": "index" }
          ]
        }
      };
      fixture.detectChanges();

    }));

    it("receives the block and the owner as inputs", () => {
      fixture.detectChanges();
      expect(component.block.type).toEqual("MenuBlock");
      expect(component.owner.name).toEqual("profile-name");
    });

    it("initialize linksEnabled variable", () => {
      const profileLinks = [
        <MenuBlockLink>{ title: 'Activities', controller: 'profile', action: 'activities' },
        <MenuBlockLink>{ title: 'About', controller: 'profile', action: 'about' }
      ];
      for (let i = 0; i < component.linksEnabled.length; i++) {
        expect(component.linksEnabled[i]).toEqual(jasmine.objectContaining(profileLinks[i]));
      }
    });

    it("initialize linksAvailable variable", () => {
      const profileLinks = [
        <MenuBlockLink>{ title: 'Communities', controller: 'memberships', action: 'index' },
        <MenuBlockLink>{ title: 'People', controller: 'friends', action: 'index' }
      ];
      for (let i = 0; i < component.linksAvailable.length; i++) {
        expect(component.linksAvailable[i]).toEqual(jasmine.objectContaining(profileLinks[i]));
      }
    });

    it("generate correct url for about", () => {
      const expectedUrl = ['/profile', 'profile-name', 'about'];
      const menuLink = Object.assign(new MenuBlockLink(), { "title": "About", "controller": "profile", "action": "about" })
      expect(component.makeUrl(menuLink)).toEqual(expectedUrl);
    });

    it("generate correct url for activities", () => {
      const expectedUrl = ['/profile', 'profile-name', 'activities'];
      const menuLink = Object.assign(new MenuBlockLink(), { "title": "Activities", "controller": "profile", "action": "activities" })
      expect(component.makeUrl(menuLink)).toEqual(expectedUrl);
    });

    it("generate correct url for communities", () => {
      const expectedUrl = ['/profile', 'profile-name', 'communities'];
      const menuLink = Object.assign(new MenuBlockLink(), { "title": "Communities", "controller": "memberships", "action": "index" })
      expect(component.makeUrl(menuLink)).toEqual(expectedUrl);
    });

    it("generate correct url for friends", () => {
      const expectedUrl = ['/profile', 'profile-name', 'friends'];
      const menuLink = Object.assign(new MenuBlockLink(), { "title": "People", "controller": "friends", "action": "index" })
      expect(component.makeUrl(menuLink)).toEqual(expectedUrl);
    });

    it("generate correct url for members", () => {
      const expectedUrl = ['/profile', 'profile-name', 'members'];
      const menuLink = Object.assign(new MenuBlockLink(), { "title": "Members", "controller": "profile_members", "action": "index" })
      expect(component.makeUrl(menuLink)).toEqual(expectedUrl);
    });

    it("generate correct url for configurations", () => {
      const expectedUrl = ['/myprofile', 'profile-name'];
      const menuLink = Object.assign(new MenuBlockLink(), { "title": "Configurations", "controller": "profile_editor", "action": "index" })
      expect(component.makeUrl(menuLink)).toEqual(expectedUrl);
    });

    it("remove link from enabled links after removeLink was called", () => {
      const amount = component.linksEnabled.length;
      component.removeLink(component.linksEnabled[0])
      expect(component.linksEnabled.length).toEqual(amount - 1);
    });

    it("add link in available links after removeLink was called", () => {
      const amount = component.linksAvailable.length;
      component.removeLink(component.linksEnabled[0])
      expect(component.linksAvailable.length).toEqual(amount + 1);
    });

    it("should call updateBlock after removeLink was called", () => {
      spyOn(component, "updateBlock").and.callThrough();
      component.removeLink(component.linksEnabled[0])
      expect(component.updateBlock).toHaveBeenCalled();
    });

    it("add link in enabled links after addLink was called", () => {
      const amount = component.linksEnabled.length;

      component.addLink(component.linksAvailable[0])
      expect(component.linksEnabled.length).toEqual(amount + 1);
    });

    it("remove link in available links after addLink was called", () => {
      const amount = component.linksAvailable.length;
      component.addLink(component.linksAvailable[0])
      expect(component.linksAvailable.length).toEqual(amount - 1);
    });

    it("should call updateBlock after addLink was called", () => {
      spyOn(component, "updateBlock").and.callThrough();
      component.addLink(component.linksAvailable[0])
      expect(component.updateBlock).toHaveBeenCalled();
    });

    it("add a new link to enabled list", () => {
      const article = <Article>{ title: 'Article 2', path: 'article-2' };
      const amount = component.linksEnabled.length;
      component.addArticle(article);
      expect(component.linksEnabled.length).toEqual(amount + 1);
    });

    it("should create a link with title equal to article's title", () => {
      const article = <Article>{ title: 'Article 2', path: 'article-2' };
      const amount = component.linksEnabled.length;
      component.addArticle(article);
      expect(_.last(component.linksEnabled).title).toEqual(article.title);
    });

    it("should create a link with path equal to article's path", () => {
      const article = <Article>{ title: 'Article 2', path: 'article-2' };
      const amount = component.linksEnabled.length;
      component.addArticle(article);
      expect(_.last(component.linksEnabled).path).toEqual(article.path);
    });

    it("should call addLink after addArticle was called", () => {
      const article = <Article>{ title: 'Article 2', path: 'article-2' };

      spyOn(component, "addLink").and.callThrough();
      component.addArticle(article)
      expect(component.addLink).toHaveBeenCalled();
    });

    it("should set searchToken to null after addArticle was called", () => {
      const article = <Article>{ title: 'Article 2', path: 'article-2' };

      component.searchToken = 'some content'

      component.addArticle(article)
      expect(component.searchToken).toBeNull();
    });

  });

});
