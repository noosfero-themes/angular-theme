import { Subscription } from 'rxjs/Subscription';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { DragulaService } from 'ng2-dragula';
import { Component, Input, Inject, HostListener, ElementRef, ViewChild, ViewEncapsulation, OnInit, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ArticleService } from '../../../services/article.service';
import { TranslatorService } from '../../../shared/services/translator.service';
import { Article } from '../../../models/article.model';
import { MenuBlockLink } from '../../../models/menu-block-link.model';
import * as _ from "lodash";
import { BlockComponentInterface } from '../block-content.component';

@Component({
  selector: "noosfero-menu-block",
  templateUrl: './menu-block.html',
  styleUrls: ['./menu-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MenuBlockComponent implements OnInit, BlockComponentInterface {

  @Input() block: any;
  @Input() owner: any;
  @Input() designMode: boolean;
  @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();


  @ViewChild("popover") popover;

  bagId: string;
  linksAvailable: MenuBlockLink[];
  linksEnabled: MenuBlockLink[];
  listOfArticles: Article[];
  searchToken: string;

  constructor(private elementRef: ElementRef,
    private translatorService: TranslatorService,
    private articleService: ArticleService,
    private dragulaService: DragulaService) {

    this.listOfArticles = [];
    this.searchToken = '';
    this.listOfArticles = Observable.create((observer: any) => {
      observer.next(this.searchToken);
    }).mergeMap((token: string) => this.searchArticle(token));
  }

  ngOnInit() {
    this.bagId = 'menu-block-bag-' + this.block.id;

    const bag: any = this.dragulaService.find(this.bagId);
    if (bag !== undefined) this.dragulaService.destroy(this.bagId);

    this.dragulaService.setOptions(this.bagId, {
      invalid: () => {
        return !this.designMode;
      },
      moves: (el, container, handle) => {
        return this.designMode;
      }
    });

    this.dragulaService.dropModel.subscribe((value) => {
      this.updateBlock();
    });


    this.linksEnabled = _.flatMap(this.block.api_content.enabled_items, function (object) {
      return Object.assign(new MenuBlockLink(), object)
    })

    this.block.hide = _.isEmpty(this.linksEnabled) ? true : false;
    this.linksAvailable = _.flatMap(this.block.api_content.available_items, function (object) {
      return Object.assign(new MenuBlockLink(), object)
    })
    this.linksAvailable = this.linksAvailable ? _.differenceWith(this.linksAvailable, this.linksEnabled, _.isEqual) : [];
  }

  makeUrl(link: MenuBlockLink) {
    const urlMapping: any = {
      'profile_about': ['/profile', this.owner.identifier, 'about'],
      'profile_activities': ['/profile', this.owner.identifier, 'activities'],
      'memberships_index': ['/profile', this.owner.identifier, 'communities'],
      'friends_index': ['/profile', this.owner.identifier, 'friends'],
      'profile_members_index': ['/profile', this.owner.identifier, 'members'],
      'profile_editor_index': ['/myprofile', this.owner.identifier],
      '': '/' + this.owner.identifier + '/' + link.path
    };

    return urlMapping[link.translationKey()];
  }

  removeLink(link) {
    this.linksEnabled.splice(this.linksEnabled.indexOf(link), 1);
    this.linksAvailable.push(link);
    this.updateBlock();
  }

  addLink(link) {
    this.linksAvailable.splice(this.linksAvailable.indexOf(link), 1);
    this.linksEnabled.push(link);
    this.updateBlock();
  }

  // FIXME this is a hack made to force the angular to identify changes on block
  updateBlock() {
    this.block.api_content.enabled_items = this.linksEnabled;
  }

  searchArticle(key: any) {
    const filters = { search: key, per_page: 10 };
    return this.articleService.getByProfile(this.owner, filters).map(result => {
      if (result.body.length === 0) {
        return [{ none: true, title: this.translatorService.translate("profile.members.invitations.none") }];
      } else {
        return result.body;
      }
    });
  }

  addArticle(article) {
    this.searchToken = null;
    if (article.none) {
      return null;
    }
    const articleLink = new MenuBlockLink();
    articleLink.path = article.path;
    articleLink.title = article.title;
    this.addLink(articleLink);

  }
}

