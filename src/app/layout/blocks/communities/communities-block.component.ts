import { Component, Inject, Input, Output, EventEmitter, ViewEncapsulation, OnInit, OnChanges } from '@angular/core';
import { BlockService } from '../../../services/block.service';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';
import { BlockComponentInterface } from '../block-content.component';

@Component({
  selector: "noosfero-communities-block",
  templateUrl: './communities-block.html',
  styleUrls: ['./communities-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CommunitiesBlockComponent implements OnInit, OnChanges, BlockComponentInterface {

  @Input() block: Block;
  @Input() owner: Profile;
  @Input() designMode: boolean;
  @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  profiles: any = [];
  public loading = false;

  constructor(private blockService: BlockService) { }

  ngOnInit() {
    if (!this.loading) {
      this.getBlockContent()
    }
  }

  getBlockContent() {
    this.loading = true;
    const limit: number = ((this.block && this.block.settings) ? this.block.settings.limit : null) || 4;
    if (this.block.api_content) {
      this.profiles = this.block.api_content['communities'];
      this.loading = false;
    } else {
      this.blockService.getBlock(this.block).subscribe((block: Block) => {
        this.block.api_content = block.api_content;
        this.profiles = this.block.api_content.communities;
        this.loading = false;
      });
    }

  }

  isEnvironment() {
    return this.owner && this.owner.type === "Environment";
  }

  ngOnChanges() {
    if (!this.loading) {
      this.getBlockContent()
    }
  }
}
