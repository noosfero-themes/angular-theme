import { EnvironmentService } from '../../../services/environment.service';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { tick, async, fakeAsync, TestBed, ComponentFixture } from '@angular/core/testing';
import * as helpers from '../../../../spec/helpers';
import { TagsCloudBlockComponent } from './tags-cloud-block.component';
import { Observable } from 'rxjs/Observable';

describe("Components", () => {
  describe("Tags Block Component", () => {
    const mocks = helpers.getMocks();

    let fixture: ComponentFixture<TagsCloudBlockComponent>;
    let component: TagsCloudBlockComponent;

    const environmentService = jasmine.createSpyObj("environmentService", ["getCurrentEnvironment", "getTags"]);

    environmentService.getCurrentEnvironment = jasmine.createSpy("getCurrentEnvironment").and.returnValue(
      { id: 1, name: 'Noosfero', host: "https://noosfero.org" }
    );

    environmentService.getTags = jasmine.createSpy("getTags").and.returnValue(
      Observable.of([{ "name": "foo", "count": 10 }, { "name": "bar", "count": 20 }])
    );

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [],
        declarations: [TagsCloudBlockComponent],
        providers: [
          { provide: EnvironmentService, useValue: environmentService },
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents().then(() => {
        fixture = TestBed.createComponent(TagsCloudBlockComponent);
        component = fixture.componentInstance;
      });
    }));

    it("get tags from the environment service", fakeAsync(() => {
      fixture.detectChanges();
      tick();
      expect(environmentService.getTags).toHaveBeenCalled();
      expect(component.tags).toEqual([{ text: "foo", weight: 10, link: '/search?tag=foo' }, { text: "bar", weight: 20, link: '/search?tag=bar' }]);
    }));
  });
});
