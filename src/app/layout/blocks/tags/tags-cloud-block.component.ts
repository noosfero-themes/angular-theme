import { Inject, Component, Input, ViewEncapsulation, OnInit, EventEmitter, Output } from '@angular/core';
import { CloudData } from 'angular-tag-cloud-module';
import { EnvironmentService } from '../../../services/environment.service';
import { Environment } from '../../../models/environment.model';
import { Tag } from '../../../models/tag.model';
import { Block } from '../../../models/block.model';
import { BlockComponentInterface } from '../block-content.component';

@Component({
  selector: "noosfero-tags-cloud-block",
  templateUrl: './tags-cloud-block.html',
  styleUrls: ['./tags-cloud-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TagsCloudBlockComponent implements OnInit, BlockComponentInterface {

  @Input() block: Block;
  @Input() owner: Environment;
  @Input() designMode: boolean;
  @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  private environment: Environment;
  public loading = false;

  tags: Array<CloudData> = [];

  constructor(private environmentService: EnvironmentService) { }

  ngOnInit() {
    this.loadTags();
  }

  loadTags() {
    this.loading = true;

    this.environment = this.environmentService.getCurrentEnvironment();
    this.environmentService.getTags(this.environment).subscribe((tags: Tag[]) => {
      this.tags = tags.map((tag: Tag) => {
        return { text: tag.name, weight: tag.count, link: `/search?tag=${tag.name}` };
      });
      this.loading = false;
    });
  }
}
