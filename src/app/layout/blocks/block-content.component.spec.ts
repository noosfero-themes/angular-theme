import { By } from '@angular/platform-browser';
import { BlockContentComponent } from './block-content.component';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import * as helpers from '../../../spec/helpers';
import { Block } from '../../models/block.model';
import { Profile } from '../../models/profile.model';
import * as blockTypes from '.';
import { MainBlockComponent } from './main/main-block.component';

import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BlockService } from '../../services/block.service';

describe("Components", () => {
  describe("Block Component", () => {
    let fixture: ComponentFixture<BlockContentComponent>;
    let component: BlockContentComponent;
    const mocks = helpers.getMocks();

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [BlockContentComponent, MainBlockComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          { provide: BlockService, useValue: mocks.blockService },
        ],
      }).overrideModule(BrowserDynamicTestingModule, {
        set: {
          entryComponents: [MainBlockComponent],
        }
      });
      fixture = TestBed.createComponent(BlockContentComponent);
      component = fixture.componentInstance;
    }));

    it("renders a component which matches to the block type", () => {
      component.block = <Block>{ type: 'MainBlock', api_content: {} };
      component.owner = <Profile>{ name: 'profile-name' };
      fixture.detectChanges();

      expect(component.block.type).toEqual("MainBlock");
      expect(component).toBeTruthy();
    });
  });
});
