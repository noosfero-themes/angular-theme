import { Component, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/article.model';
import * as _ from "lodash";

@Component({
  selector: "choose-article",
  templateUrl: './choose-article.html',
  styleUrls: ['./choose-article.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChooseArticleComponent {

  @Output() onChoose = new EventEmitter<Article[]>();
  permissionAction = "allow_edit_design,edit_environment_design";
  public loading = false;

  articles: any[] = [];
  chosenArticles: Article[] = [];
  query: string;
  tag: string;
  perPage = 100;
  currentPage = 0;

  constructor(private articleService: ArticleService) {
    this.initializeParameters();
  }

  initializeParameters() {
    this.query = '';
    this.chosenArticles = [];
  }

  searchArticle() {
    this.initializeParameters();
    const filters = {
      per_page: this.perPage,
      page: this.currentPage,
      content_type: "CommentParagraphPlugin::Discussion"
    };
    if (_.isNil(this.articles) || _.isEmpty(this.articles)) {

      this.loading = true;
      this.articleService.search(filters).subscribe((response: any) => {
        this.loading = false;
        this.articles = response.body;
      });
    }

  }

  save(modal: any) {
    this.onChoose.next(this.chosenArticles);
    modal.hide();
  }

  chooseArticle(article) {
    this.query = '';
    this.chosenArticles.push(article);
    const index = this.articles.indexOf(article, 0);
    this.articles.splice(index, 1);
  }

  remove(article: Article) {
    const index = this.chosenArticles.indexOf(article, 0);
    this.chosenArticles.splice(index, 1);
    this.articles.push(article);
  }


}
