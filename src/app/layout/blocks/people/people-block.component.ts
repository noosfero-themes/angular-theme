import { Component, Inject, Input, ViewEncapsulation, OnInit, EventEmitter, Output } from '@angular/core';
import { BlockService } from '../../../services/block.service';
import { Block } from '../../../models/block.model';
import { Environment } from '../../../models/environment.model';
import { BlockComponentInterface } from '../block-content.component';

@Component({
  selector: "noosfero-people-block",
  templateUrl: './people-block.html',
  styleUrls: ['./people-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PeopleBlockComponent implements OnInit, BlockComponentInterface {

  @Input() block: Block;
  @Input() owner: Environment;
  @Input() designMode: boolean;
  @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  profiles: any = [];
  public loading = false;

  constructor(private blockService: BlockService) { }

  ngOnInit() {
    if (!this.loading) {
      this.getBlockContent()
    }
  }

  getBlockContent() {
    this.loading = true;
    const limit: number = ((this.block && this.block.settings) ? this.block.settings.limit : null) || 4;
    if (this.block.api_content) {
      this.profiles = this.block.api_content['people'];
      this.loading = false;
    } else {
      this.blockService.getBlock(this.block).subscribe((block: Block) => {
        this.block.api_content = block.api_content;
        this.profiles = this.block.api_content.people;
        this.loading = false;
      });
    }
  }
}
