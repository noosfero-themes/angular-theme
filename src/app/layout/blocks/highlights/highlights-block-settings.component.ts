import { Input, Component, OnInit, OnChanges, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { BlockService } from '../../../services/block.service';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';
import { Environment } from '../../../models/environment.model';
import { DragulaService } from 'ng2-dragula';
import * as _ from "lodash";

@Component({
  selector: "noosfero-highlights-block-settings",
  templateUrl: './highlights-block-settings.html',
  styleUrls: ['./highlights-block-settings.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HighlightsBlockSettingsComponent implements OnInit, OnChanges {

  @Input() block: Block;
  @Input() owner: Profile | Environment;

  isCollapsed: any;
  images: any;
  bagId: any;

  constructor(
    private ref: ChangeDetectorRef,
    private dragulaService: DragulaService,
    private blockService: BlockService) {
  }

  ngOnInit() {
    this.bagId = 'highlights-block-bag-' + this.block.id;
    this.isCollapsed = true;
    this.updateImages();

    this.dragulaService.drag.subscribe((value) => {
      this.blockService.hideContent.next(false);

    });
  }

  ngOnChanges() {
    this.updateImages();
  }

  addSlide() {
    this.images.push({ image_src: "", title: "", position: this.images.length + 1, address: "http://" });
  }

  removeSlide(index: number) {
    this.images.splice(index, 1);
  }

  selectSlide(index: number) {
    (<any>this.block)['active'] = index;
  }

  updateImages() {
    const slides = (<any>this.block.api_content || {}).slides
    if (_.isNil(slides)) {
      this.images = [];
    } else {
      this.images = slides;
      (<any>this.block.settings).block_images = this.images;
    }
  }

  upload(data: any, slide: any) {
    this.blockService.uploadImages(this.block, [data]).subscribe((block: Block) => {
      this.blockService.blockChanged.next(block);
      block._force_update = true;
      this.blockService.blocksSaved.next([block]);
    });
  }

  getInterval() {
    return this.block && this.block.settings && this.block.settings['interval'] ? this.block.settings['interval'] : 0;
  }
}
