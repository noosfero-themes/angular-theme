import { Output, EventEmitter, ChangeDetectorRef, ViewEncapsulation, OnInit } from '@angular/core';
import { Input, Component, } from '@angular/core';
import { NavigationEnd, Router, Event, ActivatedRoute } from '@angular/router';
import { animateFactory } from 'ng2-animate';
import * as _ from "lodash";

import { ProfileComponent } from '../../profile/profile.component';
import { ProfileHomeComponent } from '../../profile/profile-home.component';
import { ActivitiesComponent } from '../../profile/activities/activities.component';
import { AuthService } from '../../services';
import { TranslatorService } from '../../shared/services/translator.service';
import { DesignModeService } from '../../shared/services/design-mode.service';
import { BlockService } from '../../services/block.service';
import { Block, BlockDisplayOptions, BlockLoggedUserOptions } from '../../models/block.model';
import { Box } from '../../models/box.model';
import { Profile } from '../../models/profile.model';
import { Environment } from '../../models/environment.model';
import { User } from '../../models/user.model';
import { ProfileAboutComponent } from '../../profile/about/profile-about.component';

@Component({
  selector: 'noosfero-block',
  templateUrl: './block.html',
  animations: [animateFactory(500, 0, 'ease-in')],
  styleUrls: ['./block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BlockComponent implements OnInit {

  @Input() block: Block;
  @Input() box: Box;
  @Input() owner: Profile | Environment;
  @Output() onRemove = new EventEmitter<Block>();


  currentUser: User;
  isHomepage = true;
  designMode = false;
  columns = {}
  animation: string;
  canDisplayOnPreview = true;
  hideBlock = false;
  hideContent = false;

  date = null

  constructor(private ref: ChangeDetectorRef,
    private authService: AuthService,
    private blockService: BlockService,
    private translatorService: TranslatorService,
    private designModeService: DesignModeService,
    private router: Router, private route: ActivatedRoute) {

    this.date = Date.now()

    this.currentUser = this.authService.currentUser();
    this.authService.loginSuccess.subscribe((user: User) => {
      this.currentUser = user;
      this.verifyHomepage();
    });
    this.authService.logoutSuccess.subscribe(() => {
      this.currentUser = null;
      this.verifyHomepage();
    });

    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) this.verifyHomepage();
    });
    this.designModeService.onToggle.subscribe((designModeOn: boolean) => {
      this.designMode = designModeOn;
    });
  }

  ngOnInit() {

    this.blockService.blocksSaved.subscribe((remoteBlocks: Block[]) => {

      let remoteBlock = null;
      if (this.block.id === null) {
        remoteBlock = _.find(remoteBlocks, { 'position': this.block.position, 'box_id': this.block.box_id })
      } else {
        remoteBlock = _.find(remoteBlocks, { 'id': this.block.id })
      }
      this.block = remoteBlock ? remoteBlock : this.block;

    });

    this.verifyHomepage();
    this.designMode = this.designModeService.isInDesignMode();
    if (!this.block.settings) this.block.settings = {};

    if (this.block.settings.visualization) {
      this.columns = {};
    }

    if (!this.block.id) {
      this.animation = "zoomInDown";
    }
    if (this.block.hide) {
      this.hideBlock = true;
    }

    this.blockService.hideContent.subscribe((hide) => {
      this.hideContent = hide ? true : false;
    });
  }

  resizeColumn(columns) {
    this.block.settings.visualization = {};
    this.block.settings.visualization['columns'] = columns;

  }

  canDisplay() {
    if (this.designMode) return true;
    if (this.block._destroy) return false;

    return this.visible() && this.displayToUser() &&
      this.displayOnLanguage(this.translatorService.currentLanguage()) &&
      !this.hideBlock;
  }

  canDisplayOnPreviewChangedOnChild(canDisplayOnPreview: boolean) {
    this.canDisplayOnPreview = canDisplayOnPreview;
    this.ref.detectChanges();
  }

  hideBlockChanged(hideBlock: boolean) {
    this.hideBlock = hideBlock;
    this.ref.detectChanges();
  }

  blockClass() {
    if (!this.block || !this.block.type) return null;
    return this.block.type.toLowerCase().replace(/::/, '-');
  }

  protected visible() {
    const display = this.block.settings ? (<any>this.block.settings)['display'] : null;
    return !display || ((this.isHomepage ? display !== BlockDisplayOptions.EXCEPT_HOME_PAGE : display !== BlockDisplayOptions.HOME_PAGE_ONLY) && display !== BlockDisplayOptions.NEVER);
  }

  protected displayToUser() {
    const displayUser = this.block.settings ? (<any>this.block.settings)['display_user'] : null;

    return !displayUser || displayUser === BlockLoggedUserOptions.ALL ||
      (this.currentUser ? displayUser === BlockLoggedUserOptions.LOGGED : displayUser === BlockLoggedUserOptions.NOT_LOGGED);
  }

  protected displayOnLanguage(language: string) {
    const displayLanguage = this.block.settings ? (<any>this.block.settings)['language'] : null;
    return !displayLanguage || displayLanguage === "all" ||
      language === displayLanguage;
  }

  protected verifyHomepage() {
    if (this.owner && this.owner.type !== "Environment") {
      const profile = <Profile>this.owner;
      if (profile.homepage) {
        this.isHomepage = this.router.url === profile.homepage;
      } else {
        const currentComponent = (_.isEmpty(this.route.snapshot.children)) ? this.route.snapshot.component : this.route.snapshot.children[0].component;
        const regex = "[profile]?/" + profile.identifier + "$/?";
        const isInitialUrl = !_.isNil(this.router.url.match(regex));

        const isInitialComponent = [ActivitiesComponent, ProfileComponent, ProfileHomeComponent, ProfileAboutComponent].indexOf(<any>currentComponent) >= 0;
        this.isHomepage = isInitialComponent && isInitialUrl;
      }
    } else {
      this.isHomepage = this.router.url === "/";
    }
  }

  markForDeletion() {
    this.block._destroy = true;
    this.animation = "zoomOutUp";
    this.onRemove.next(this.block);
    this.ref.detectChanges();
  }

  canDelete() {
    return this.block.type !== 'MainBlock';
  }

  updateText(event) {
    this.block.title = event;
  }
}
