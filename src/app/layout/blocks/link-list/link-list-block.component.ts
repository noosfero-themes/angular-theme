import { Component, Input, Inject, OnChanges, ViewEncapsulation, OnInit, EventEmitter, Output } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { BlockComponentInterface } from '../block-content.component';
import { BlockService } from '../../../services/block.service';

@Component({
  selector: "noosfero-link-list-block",
  templateUrl: './link-list-block.html',
  styleUrls: ['./link-list-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LinkListBlockComponent implements OnChanges, OnInit, BlockComponentInterface {

  @Input() block: any;
  @Input() owner: any;
  @Input() designMode: boolean;
  @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  links: any;
  bagId: any;

  constructor(private dragulaService: DragulaService, private blockService: BlockService) { }

  ngOnInit() {
    this.bagId = 'link-list-block-bag-' + this.block.id;

    const bag: any = this.dragulaService.find(this.bagId);
    if (bag !== undefined) this.dragulaService.destroy(this.bagId);


    this.dragulaService.setOptions(this.bagId, {
      invalid: () => {
        return !this.designMode;
      },
      moves: (el, container, handle) => {
        return this.designMode;
      }
    });

    this.dragulaService.drag.subscribe((value) => {
      this.blockService.hideContent.next(false);

    });

    this.updateLinks();

  }

  updateLinks() {
    this.links = this.block.api_content.links;
    this.applyVisibility();
  }

  applyVisibility() {
    if (<any>this.links == null || <any>this.links.length === 0) {
      this.hideBlockChanged.emit(true);
    } else {
      this.hideBlockChanged.emit(false);
    }
  }

  ngOnChanges() {
    this.updateLinks();
  }

  updateLink(i: number, item: any) {
    this.links[i].name = item.name;
    this.links[i].address = item.address;
  }

  updateIcon(i: number, icon: string) {
    this.links[i].icon = icon;
  }

  addLink() {
    this.links.push({ name: "", address: "http://", icon: "fa-file-o", position: this.links.length + 1 });
  }

  removeLink(index: number) {
    this.links.splice(index, 1);
  }



}
