import { Input, Inject, Component, ViewEncapsulation, OnInit } from '@angular/core';

import { ArticleService } from '../../../services/article.service';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';
import { Article } from '../../../models/article.model';
import * as _ from "lodash";

@Component({
  selector: "noosfero-display-content-block",
  templateUrl: './display-content-block.html',
  styleUrls: ['./display-content-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DisplayContentBlockComponent implements OnInit {

  @Input() block: Block;
  @Input() owner: Profile;
  @Input() designMode: boolean;

  profile: Profile;
  articles: Article[];
  sections = [];

  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.profile = this.owner;
    const limit = ((this.block && this.block.settings) ? this.block.settings.limit : null) || 5;
    this.sections = _.flatMap(this.block.settings.sections, function (object) {
      return object.checked ? object.value : null;
    })
    this.sections = _.compact(this.sections);

    this.articleService.getByProfile(this.profile, { per_page: limit }).subscribe((result) => {
      this.articles = result.body;
    });
  }

  hasContent(content: string): boolean {
    return _.isNil(content) ? false : (content.length > 0 ? true : false);
  }

  private display(sectionName: string): boolean {
    return this.sections.indexOf(sectionName) >= 0;
  }

}
