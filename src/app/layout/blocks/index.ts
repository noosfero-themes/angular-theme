import { CommunitiesBlockComponent } from './communities/communities-block.component';
import { DisplayContentBlockComponent } from './display-content/display-content-block.component';
import { HighlightsBlockComponent } from './highlights/highlights-block.component';
import { LinkListBlockComponent } from './link-list/link-list-block.component';
import { LoginBlockComponent } from './login-block/login-block.component';
import { MainBlockComponent } from './main/main-block.component';
import { MembersBlockComponent } from './members/members-block.component';
import { MenuBlockComponent } from './menu/menu-block.component';
import { PeopleBlockComponent } from './people/people-block.component';
import { ProfileImageBlockComponent } from './profile-image/profile-image-block.component';
import { RawHTMLBlockComponent } from './raw-html/raw-html-block.component';
import { RecentDocumentsBlockComponent } from './recent-documents/recent-documents-block.component';
import { StatisticsBlockComponent } from './statistics/statistics-block.component';
import { TagsCloudBlockComponent } from './tags/tags-cloud-block.component';
import { InterestTagsBlockComponent } from './interest-tags/interest-tags-block.component';

import * as plugins from '../../../plugins';

const coreBlocks = [CommunitiesBlockComponent, DisplayContentBlockComponent, HighlightsBlockComponent,
  LinkListBlockComponent, LoginBlockComponent, MainBlockComponent, MembersBlockComponent, MenuBlockComponent,
  PeopleBlockComponent, ProfileImageBlockComponent, RawHTMLBlockComponent, RecentDocumentsBlockComponent,
  StatisticsBlockComponent, TagsCloudBlockComponent, InterestTagsBlockComponent];

export let blocks = {}
plugins.ng2MainComponents.concat(coreBlocks).forEach((plugin: any) => {
  blocks[plugin.name] = plugin;
});
