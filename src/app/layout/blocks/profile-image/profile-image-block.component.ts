import { ProfileImageComponent } from '../../../profile/image/profile-image.component';
import { Component, Inject, Input, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';
import { BlockComponentInterface } from '../block-content.component';

@Component({
  selector: "noosfero-profile-image-block",
  templateUrl: './profile-image-block.html',
  styleUrls: ['./profile-image-block.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class ProfileImageBlockComponent implements BlockComponentInterface {

  @Input() block: Block;
  @Input() owner: Profile;
  @Input() designMode: boolean;
  @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideBlockChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

}
