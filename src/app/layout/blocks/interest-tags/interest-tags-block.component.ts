import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/profile.service';
import { PersonService } from '../../../services/person.service';
import { Tag } from '../../../models/tag.model';

@Component({
  selector: "noosfero-interest-tags-block",
  templateUrl: './interest-tags-block.html',
  styleUrls: ['./interest-tags-block.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InterestTagsBlockComponent implements OnInit {

  @Input() block: any;
  @Input() owner: any;
  @Input() designMode: boolean;

  profile: any;
  tags: Tag[];

  constructor(private profileService: ProfileService) { }

  ngOnInit() {
    this.profile = this.owner;
    this.tags = [];
    this.profileService.getTags(this.owner).subscribe((tags: Tag[]) => {
      this.tags = tags;
      this.block.hide = (<any>this.tags.length === 0);
    });
  }
}
