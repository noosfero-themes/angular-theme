import { TranslateModule } from '@ngx-translate/core';
import { BlockService } from '../../../services/block.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { PopoverModule } from 'ngx-bootstrap';
import { BlockEditionComponent } from './block-edition.component';
import * as helpers from '../../../../spec/helpers';
import { Profile } from '../../../models/profile.model';
import { Box } from '../../../models/box.model';
import { Block, BlockDisplayOptions, BlockLoggedUserOptions } from '../../../models/block.model';

describe("Components", () => {

  describe("Block Edition Component", () => {

    const mocks = helpers.getMocks();
    let fixture: ComponentFixture<BlockEditionComponent>;
    let component: BlockEditionComponent;

    beforeEach(async(() => {
      spyOn(mocks.blockService, "blockChanged");
      spyOn(mocks.blockService.blockChanged, "next");
      TestBed.configureTestingModule({
        imports: [PopoverModule.forRoot(), TranslateModule.forRoot()],
        declarations: [BlockEditionComponent],
        schemas: [NO_ERRORS_SCHEMA],
        providers: [
          { provide: BlockService, useValue: mocks.blockService },
        ]
      });

      fixture = TestBed.createComponent(BlockEditionComponent);
      component = fixture.componentInstance;
      component.owner = <Profile>{ id: 1, name: 'profile-name', identifier: 'profile-name' };
      component.block = <Block>{ id: 1, settings: <any>{} };
      component.box = <Box>{ id: 2 };
      fixture.detectChanges();
    }));

    it("return true for first option when setting is null", () => {
      expect(component.isOptionSelected("display", "always")).toBeTruthy();
    });

    it("return false for other options when setting is null", () => {
      expect(component.isOptionSelected("display", "home_page_only")).toBeFalsy();
    });

    it("change block setting when select an option", () => {
      component.selectOption("display", "never");
      expect(component.isOptionSelected("display", "never")).toBeTruthy();
    });

    it("emit change event when an attribute was modified", () => {
      component.block.title = "changed";
      fixture.detectChanges();
      expect(mocks.blockService.blockChanged.next).toHaveBeenCalledWith(component.block);
    });

    it("emit change event when an setting attribute was modified", () => {
      (<any>component.block.settings).display = "never";
      fixture.detectChanges();
      expect(mocks.blockService.blockChanged.next).toHaveBeenCalledWith(component.block);
    });

    it("not emit change event with block id when an setting attribute was not modified", () => {
      (<any>component.lastBlockChanges.settings).display = "never";
      (<any>component.block.settings).display = "never";
      fixture.detectChanges();
      expect(mocks.blockService.blockChanged.next).not.toHaveBeenCalledWith(component.block);
    });

    it("add never option for blocks of other type than main block", () => {
      expect(component.options.display).toContain('never');
    });

    it("not add never option for main block", () => {
      fixture = TestBed.createComponent(BlockEditionComponent);
      component = fixture.componentInstance;
      component.block = <Block>{ id: 1, settings: <any>{}, type: "MainBlock" };
      component.box = <Box>{ id: 2 };
      fixture.detectChanges();
      expect(component.options.display).not.toContain('never');
    });
  });

});
