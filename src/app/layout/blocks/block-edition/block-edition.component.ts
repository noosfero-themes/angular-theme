import { SimpleChanges, Inject, Input, Component, HostListener, ElementRef, ViewChild, ViewEncapsulation, OnInit, DoCheck, OnChanges, OnDestroy, ChangeDetectorRef } from '@angular/core';
import * as _ from "lodash";

import { BlockService } from '../../../services/block.service';
import { Block, BlockDisplayOptions, BlockLoggedUserOptions } from '../../../models/block.model';
import { Box } from '../../../models/box.model';
import { Profile } from '../../../models/profile.model';
import { Environment } from '../../../models/environment.model';

@Component({
  selector: 'noosfero-block-edition',
  templateUrl: './block-edition.html',
  styleUrls: ['./block-edition.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BlockEditionComponent implements OnInit, DoCheck, OnChanges {


  @Input() block: Block;
  @Input() box: Box;
  @Input() owner: Profile | Environment;
  @ViewChild("popover") popover: any;

  lastBlockChanges = null;

  options = {
    display: _.values(BlockDisplayOptions),
    display_user: _.values(BlockLoggedUserOptions),
  };

  subscription = null;

  constructor(private ref: ChangeDetectorRef, private elementRef: ElementRef, private blockService: BlockService) { }

  ngDoCheck() {
    this.updateLastBlockChanges();

    if (this.checkHasChanges()) {
      this.lastBlockChanges = _.cloneDeep(this.block);
      this.blockService.blockChanged.next(this.block)
    }
  }

  ngOnChanges() {
    this.updateLastBlockChanges();
  }

  updateLastBlockChanges() {
    if (this.lastBlockChanges && this.block._force_update) {
      this.clearForceUpdate();
      this.lastBlockChanges = this.block;
    }
  }

  clearForceUpdate() {
    delete this.block._force_update;
  }

  ngOnInit() {
    this.clearForceUpdate();

    if (this.block.id || !_.isNil(this.block._add)) {
      this.lastBlockChanges = _.cloneDeep(this.block);
    }

    if (this.block.type === 'MainBlock')
      this.options.display = this.options.display.filter(obj => obj !== BlockDisplayOptions.NEVER);
  }

  selectOption(optionKey: string, option: string) {
    (<any>this.block.settings)[optionKey] = option;
  }


  checkHasChanges() {
    return !_.isEqual(this.block, this.lastBlockChanges);
  }

  isOptionSelected(optionKey: string, option: string) {
    return (<any>this.block.settings)[optionKey] === option ||
      (<any>this.block.settings)[optionKey] == null && this.options[optionKey].indexOf(option) === 0;
  }

  optionsKeys() {
    return Object.keys(this.options);
  }

  optionsValues(optionKey: string) {
    return this.options[optionKey];
  }

  @HostListener('document:click', ['$event'])
  onClick($event: any) {
    if (this.popover && !this.elementRef.nativeElement.contains($event.target)) {
      this.popover.hide();
    }
  }
}
