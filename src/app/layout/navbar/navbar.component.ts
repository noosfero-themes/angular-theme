import { HeaderService } from '../../shared/services/header.service';
import { Component, Inject, EventEmitter, Input, ViewEncapsulation } from '@angular/core';
import { AuthService, AuthEvents } from '../../services';
import { EnvironmentService } from '../../services/environment.service';
import { ProfileService } from '../../services/profile.service';
import { DesignModeTogglerComponent } from '../design-mode-toggler/design-mode-toggler.component';
import { SessionService } from '../../services/session.service';
import { Environment } from '../../models/environment.model';
import { User } from '../../models/user.model';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: "noosfero-navbar",
  templateUrl: './navbar.html',
  styleUrls: ['./navbar.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NavbarComponent {

  private currentUser: User;
  public environment = new Environment();
  showLoginModal = false;

  constructor(headerService: HeaderService, public authService: AuthService,
    private environmentService: EnvironmentService, private profileService: ProfileService,
    private router: Router) {
    this.currentUser = this.authService.currentUser();

    this.authService.loginSuccess.subscribe((user: User) => {
      this.showLoginModal = false;
      this.currentUser = user;
    });

    this.environmentService.environmentChangeEvent.subscribe(environment => {
      this.environment = environment;
    });

    this.authService.logoutSuccess.subscribe((user: User) => {
      const profile = this.profileService.getCurrentProfile()
      if (profile) {
        this.router.navigate([profile.identifier]);
      } else {
        this.router.navigate(['/']);
      }
      this.currentUser = null;
    });
  }

  logout() {
    this.authService.logout(this.currentUser).subscribe(() => {
      this.authService.logoutSuccessCallback(this.currentUser);
    });
  };

  openLogin() {
    this.showLoginModal = true;
  }

  activate() {
    if (!this.currentUser) {
      this.openLogin();
    }
  }
}
