import * as _ from "lodash";

export class MenuBlockLink {

  title: string;
  public controller: string;
  public action: string;
  public path: string;

  public translationKey(): string {
    if (_.isNil(this.controller) || _.isNil(this.action)) {
      return ''
    } else {
      return _.join([this.controller, this.action], '_');
    }
  }

  public isArticle(): boolean {
    return _.isNil(this.controller);
  }

}

