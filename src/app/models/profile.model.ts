import { Box } from './box.model'

export class Profile {

  permissions: string[];
  id: number;
  identifier: string;
  created_at: string;
  type: string;
  name: string;
  additional_data?: any;
  homepage: string;
  custom_header: string;
  custom_footer: string;
  layout_template: string;
  top_image: any;
  image: any;
  boxes: Box[];
  theme: string;
}
