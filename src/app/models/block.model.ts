import { Image } from './image.model'
import { Box } from './box.model'
import { BlockDefinition } from './block-definition.model'

export class Block {

  id: number;
  _temp_id: string;
  settings: any;
  limit: number;
  api_content: any;
  hide: boolean;
  title: string;
  type: string;
  images: Image[];
  position: number;
  permissions: string[];
  box: Box;
  box_id: number;
  definition: BlockDefinition;
  mirror_block_id: number;
  mirror: boolean;
  enabled: boolean;
  active: any;

  // Variable to indicate to rails backend that this model should be removed in nested attributes update on blocks manipulation
  _destroy: boolean;

  // Variable used to force id be null before send it to rails backend on blocks manipulation
  _add: boolean;

  // Variable used to force update of local blocks
  _force_update: boolean;

  // Used to manage blocks in movements
  box_origin_id: number;

}

export enum BlockDisplayOptions {

  ALWAYS = "always",
  HOME_PAGE_ONLY = "home_page_only",
  EXCEPT_HOME_PAGE = "except_home_page",
  NEVER = "never",

}


export enum BlockLoggedUserOptions {

  ALL = "all",
  LOGGED = "logged",
  NOT_LOGGED = "not_logged",

}

