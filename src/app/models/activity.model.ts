import { Profile } from './profile.model';

export class Activity {
  verb: string;

  user: Profile;
  params: any[];
  created_at: any;
  updated_at: any;
  target: Profile;

}

