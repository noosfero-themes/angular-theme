import { Box } from './box.model'

export class Environment {
  id: number;
  settings: any
  layout_template: string;
  signup_intro: string;
  host: string;
  name: string;
  type: string;
  boxes: Box[];
  permissions: string[];
  theme: string;
  contact_email: string;
  terms_of_use: string;
  captcha_signup_enable: boolean;
  captcha_site_key: string;
}
