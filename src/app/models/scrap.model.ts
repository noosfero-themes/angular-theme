import { Profile } from "./profile.model";
import { Person } from "./person.model";

export class Scrap {
  replies_count: number;
  scrap_id: number;
  id: number;
  content: string;
  created_at: any;
  updated_at: any;
  receiver: Profile;
  sender: Person;
}
