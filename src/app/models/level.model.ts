export enum Level {

  VISITORS = 0,
  USERS = 10,
  RELATED = 20,
  SELF = 30,
  NOBODY = 40

}
