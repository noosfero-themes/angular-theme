import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Input, Component, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { NotificationService } from '../../shared/services/notification.service';
import { EnvironmentService } from '../../services/environment.service';
import { ValidationMessageComponent } from '../../shared/components/validation-message/validation-message.component';
import { Environment } from '../../models/environment.model'
import { AuthService } from '../../services/auth.service';
import { User } from '../../models/user.model';
import { switchMap } from 'rxjs/operator/switchMap';
import { switchMapTo } from 'rxjs/operators';

@Component({
  selector: 'noosfero-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['activate.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ActivateComponent implements OnInit {
  account: any;
  environment: Environment;

  constructor(private router: Router, public authService: AuthService,
    private notificationService: NotificationService,
    private route: ActivatedRoute,
    private environmentService: EnvironmentService) {
    this.account = {};
    this.environmentService.environmentChangeEvent.subscribe(environment => {
      this.environment = environment;
    });

    this.account.activation_token = this.route.snapshot.queryParams['activation_token'];
  }

  ngOnInit() {
    this.environment = this.environmentService.getCurrentEnvironment();
  }


  activate() {
    this.authService.activate(this.account).subscribe((response: any) => {
      if (response.success) {
        this.router.navigate(['/']);
        this.notificationService.success({ title: "account.activate.success.title", message: response.message });
      } else {
        this.authService.loginSuccessCallback(response);
        this.router.navigate(['/']);
        this.notificationService.success({ title: "account.activate.success.title", message: "account.activate.success.message" });
      }
    }, (response) => {

      this.notificationService.error({ title: "account.activate.failed.title", message: "account.activate.failed.message" });

    });
  }

}
