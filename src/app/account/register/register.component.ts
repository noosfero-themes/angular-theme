import { Router } from '@angular/router';
import { Input, Component, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { NotificationService } from '../../shared/services/notification.service';
import { EnvironmentService } from '../../services/environment.service';
import { ValidationMessageComponent } from '../../shared/components/validation-message/validation-message.component';
import { Environment } from '../../models/environment.model'
import { AuthService } from '../../services/auth.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'noosfero-register',
  templateUrl: './register.component.html',
  styleUrls: ['register.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RegisterComponent implements OnInit {
  @Input() account: any;
  environment: Environment;
  showLoginModal = false;

  @ViewChild('nameErrors') nameErrors: ValidationMessageComponent;
  @ViewChild('userNameErrors') userNameErrors: ValidationMessageComponent;
  @ViewChild('emailErrors') emailErrors: ValidationMessageComponent;
  @ViewChild('passwordErrors') passwordErrors: ValidationMessageComponent;
  @ViewChild('passwordConfirmErrors') passwordConfirmErrors: ValidationMessageComponent;
  @ViewChild('captchaErrors') captchaErrors: ValidationMessageComponent;
  @ViewChild('captcha') captcha: any;

  constructor(private router: Router, public authService: AuthService,
    private notificationService: NotificationService,
    private environmentService: EnvironmentService) {
    this.account = {};
    this.environmentService.environmentChangeEvent.subscribe(environment => {
      this.environment = environment;
    });
  }

  ngOnInit() {
    this.environment = this.environmentService.getCurrentEnvironment();
  }

  captchaEnable(): boolean {
    return this.environment ? this.environment.captcha_signup_enable : true;
  }

  captchaSiteKey(): string {
    return this.environment ? this.environment.captcha_site_key : '';
  }

  openLogin() {
    this.showLoginModal = true;
  }

  signup() {
    this.authService.createAccount(this.account).subscribe((user: User) => {
      if (user.activated) {
        this.router.navigate(['/']);
        this.notificationService.success({ title: "account.register.success.title", message: "account.register.success.message" });
      } else {
        this.router.navigate(['/', 'account', 'activate'], { queryParams: { activation_token: user.activation_code } });
        this.notificationService.success({ title: "account.register.activate.title", message: "account.register.activate.message" });
      }

    }, (response) => {
      const errors = response.error;
      if (response.status === 422) {
        this.nameErrors.setBackendErrors(errors);
        this.userNameErrors.setBackendErrors(errors);
        this.emailErrors.setBackendErrors(errors);
        this.passwordErrors.setBackendErrors(errors);
        this.passwordConfirmErrors.setBackendErrors(errors);
        this.captchaErrors.setBackendErrors(errors);
        this.captcha.reset();
      } else {
        this.notificationService.error({ title: "account.register.save.failed" });
      }
    });
  }

}
